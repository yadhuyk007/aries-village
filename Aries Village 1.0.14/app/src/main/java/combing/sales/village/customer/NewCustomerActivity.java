package combing.sales.village.customer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bugsnag.android.Bugsnag;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import combing.sales.village.ariesc.R;
import combing.sales.village.database.DatabaseHandler;
import combing.sales.village.landing.LandingActivity;
import combing.sales.village.landing.TabVILLAGE;
import combing.sales.village.objects.Font;
import combing.sales.village.objects.Global;
import combing.sales.village.objects.Stacklogger;
import combing.sales.village.objects.webconfigurration;

public class NewCustomerActivity extends AppCompatActivity {

    private static final int CAMERA_REQUEST = 1888;
    private Activity activity;
    public static boolean OTP_verificationstatus;
    public static String OTP_submitTime = "";
    private static Uri mImageUri;
    private static String type = "";
    private static String mobilenumber;
    List<String> categories = new ArrayList<String>();
    Hashtable<String, String> customercategorymap = new Hashtable<String, String>();
    private static DatabaseHandler db;
    private static Button verification;
    private static ImageView tick;
    private Bitmap customerphoto;
    private Bitmap insidephoto;
    private boolean verify = false;
    private CountDownTimer yourCountDownTimer;
    private Activity mAct = NewCustomerActivity.this;
    private EditText contactnumber;
    private TextView mTextField;
    private Spinner spinner;
    List<String> HygieneSubDs = new ArrayList<>();
    List<String> HealthsSubDs = new ArrayList<>();
    List<String> villages = new ArrayList<>();
    private RelativeLayout mainlay;
    private EditText custname;
    private EditText address;
    private Spinner spinnercustcat;
    private Spinner spinsubD;
    private Spinner spinvillage;
    private AutoCompleteTextView auto;
    private Spinner villagespin;
    private int EFlag = 0;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        spinnerscrollremove();
        pref = getApplicationContext()
                .getSharedPreferences("Config",
                        MODE_PRIVATE);
        String perid = pref.getString("personid", "");
        String porgid = pref.getString("tmsOrgId", "");
        webconfigurration C = new webconfigurration(getApplicationContext());
        Bugsnag.init(getApplicationContext(),"d12422957abb0ffffd9f22a3fc46154c");
        Bugsnag.setUser(perid,porgid, C.user);
        try {
            intialisecategories();
            intialisubds();
            initialisevillage();
        } catch (JSONException e1) {
            e1.printStackTrace();
            Bugsnag.notify(e1);
        }

        final Spinner spinsubd = (Spinner) findViewById(R.id.spinsubD);
        ArrayAdapter<String> subdadapt = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, HealthsSubDs) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(12);

                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        subdadapt.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        spinsubd.setAdapter(subdadapt);

        final Spinner hsubd = (Spinner) findViewById(R.id.hygenesubD);
        ArrayAdapter<String> hsubdadapt = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, HygieneSubDs) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(12);

                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        hsubdadapt.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        hsubd.setAdapter(hsubdadapt);

        try{
            spinsubd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String subdval = spinsubd.getSelectedItem().toString();
                    String hsubdval = hsubd.getSelectedItem().toString();
                    if (hsubdval.contentEquals("SELECT") || hsubdval.contentEquals("")) {
                        if (!(subdval.contentEquals("SELECT") || subdval.contentEquals(""))) {
                            String[] parts;
                            String name="";
                            String code = "";
                            try{
                                parts = subdval.split("-");
                                name = parts[0];
                                if (parts.length>1){
                                    code = parts[1];
                                    String subdid = db.getsubdid(name, code);
                                    try {
                                        initvillagefromhealth(subdid);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                                }

                            }catch (ArrayIndexOutOfBoundsException e){
                                Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                                code="0";
                            }
                        }

//                    Spinner villagespin = (Spinner)findViewById(R.id.spinvillage);
//                    villagespin.setSelection(0);
                    }else if (subdval.contentEquals("SELECT") || subdval.contentEquals("")) {
                        if (!(hsubdval.contentEquals("SELECT") || hsubdval.contentEquals(""))) {
                            String[] parts = hsubdval.split("-");
                            String subdid="";
                            try{
                                String name = parts[0];
                                String code="";
                                if (parts.length>1){
                                    code = parts[1];
                                    subdid = db.getsubdid(name, code);
                                    try {
                                        initvillagefromhygene(subdid);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                                }

                            }catch (ArrayIndexOutOfBoundsException e){
                                Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                            }

                        }

                    } else if (!(subdval.contentEquals("SELECT") || subdval.contentEquals("")) && !(hsubdval.contentEquals("SELECT") || hsubdval.contentEquals(""))) {
                        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                        String name = "";
                        String code="";
                        String name1="";
                        String code1="";
                        String subdid="";
                        try{
                            String[] parts1 = subdval.split("-");
                            name = parts1[0];
                            if(parts1.length>1){
                                code = parts1[1];
                                subdid = db.getsubdid(name, code);
                            }else{
                                Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                            }

                            String[] parts2 = hsubdval.split("-");
                            name1 = parts2[0];
                            if (parts2.length>1){
                                code1 = parts2[1];
                                String hsubdid = db.getsubdid(name1, code1);
                                try {
                                    initialisevillage(subdid, hsubdid);
                                    Spinner villagespin = (Spinner) findViewById(R.id.spinvillage);
                                    villagespin.setSelection(0);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else{
                                Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                            }

                        }catch (ArrayIndexOutOfBoundsException e){
                            Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                        }





                    } else {
                        villages.clear();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            hsubd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String subdval = spinsubd.getSelectedItem().toString();
                    String hsubdval = hsubd.getSelectedItem().toString();
                    if (subdval.contentEquals("SELECT") || subdval.contentEquals("")) {
                        if (!(hsubdval.contentEquals("SELECT") || hsubdval.contentEquals(""))) {
                            String subdid="";
                            try{
                                String[] parts = hsubdval.split("-");
                                String name = parts[0];
                                String code="";
                                if (parts.length>1){
                                    code = parts[1];
                                    subdid = db.getsubdid(name, code);
                                    try {
                                        initvillagefromhygene(subdid);
                                    } catch (JSONException e) {
                                        e.printStackTrace();

                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                                }


                            }catch (ArrayIndexOutOfBoundsException e){
                                Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                            }


                        }

                    }else if (hsubdval.contentEquals("SELECT") || hsubdval.contentEquals("")) {
                        if (!(subdval.contentEquals("SELECT") || subdval.contentEquals(""))) {
                           String subdid="";
                            try{
                                String[] parts = subdval.split("-");
                                String name = parts[0];
                                String code = "";
                                if (parts.length>1){
                                    code = parts[1];
                                    subdid = db.getsubdid(name, code);
                                    try {
                                        initvillagefromhealth(subdid);
                                    } catch (JSONException e) {
                                        e.printStackTrace();

                                    }
                                }else{
                                    Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                                }

                            }catch (ArrayIndexOutOfBoundsException e){
                                Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                            }


                        }
                    } else if (!(subdval.contentEquals("SELECT") || subdval.contentEquals("")) && !(hsubdval.contentEquals("SELECT") || hsubdval.contentEquals(""))) {
                        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                        String subdid="";
                        String hsubdid="";
                        try {
                            String[] parts1 = subdval.split("-");
                            String name = parts1[0];
                            String code= "";
                            if (parts1.length>1){
                                code = parts1[1];
                                subdid = db.getsubdid(name, code);
                            }


                            String[] parts2 = hsubdval.split("-");
                            String name1 = parts2[0];
                            String code1 = "";
                            if (parts2.length>1){
                                code1 = parts2[1];
                                hsubdid = db.getsubdid(name1, code1);
                                try {
                                    initialisevillage(subdid, hsubdid);
                                    Spinner villagespin = (Spinner) findViewById(R.id.spinvillage);
                                    villagespin.setSelection(0);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }else{
                                Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                            }





                        }catch (ArrayIndexOutOfBoundsException e){
                            Toast.makeText(getApplicationContext(), "Please add the code for SubD", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        villages.clear();
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }catch (Exception e){
            e.printStackTrace();
            Bugsnag.notify(e);
        }

        villagespin = (Spinner) findViewById(R.id.spinvillage);
        ArrayAdapter<String> villageadapt = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, villages) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView, parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(12);

                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }
        };
        villageadapt.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        villagespin.setAdapter(villageadapt);

        Spinner spinner = (Spinner) findViewById(R.id.spinnercustcat);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, categories) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(12);

                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }

        };
        dataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        spinner.setAdapter(dataAdapter);

        contactnumber = (EditText) findViewById(R.id.ContactNumber);
        verification = (Button) findViewById(R.id.verify);
        tick = (ImageView) findViewById(R.id.tickbutton);
        OTP_verificationstatus = false;
        Button verify = (Button) findViewById(R.id.verify);
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verify();
            }
        });
        //take photo
        ImageButton photo_camera = (ImageButton) findViewById(R.id.photo_camera);
        photo_camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(NewCustomerActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NewCustomerActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                } else {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photo;
                    try {
                        photo = createTemporaryFile("picture", ".jpg");
                        photo.delete();
                    } catch (Exception e) {
                        Toast.makeText(NewCustomerActivity.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mImageUri = Uri.fromFile(photo);
                    type = "photo";
                    startActivityForResult(intent, CAMERA_REQUEST);
                }
            }
        });


        //take photo
        ImageButton insidephoto = (ImageButton) findViewById(R.id.inside_photo);
        insidephoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(NewCustomerActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NewCustomerActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                } else {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photo;
                    try {
                        photo = createTemporaryFile("picture", ".jpg");
                        photo.delete();
                    } catch (Exception e) {
                        Toast.makeText(NewCustomerActivity.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mImageUri = Uri.fromFile(photo);
                    type = "inside";
                    startActivityForResult(intent, CAMERA_REQUEST);
                }
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void spinnerscrollremove() {

        mainlay = (RelativeLayout) findViewById(R.id.mainlay);
        custname = (EditText) findViewById(R.id.custname);
        address = (EditText) findViewById(R.id.address);
        spinnercustcat = (Spinner) findViewById(R.id.spinnercustcat);
        spinvillage = (Spinner) findViewById(R.id.spinvillage);
        spinsubD = (Spinner) findViewById(R.id.spinsubD);
        mainlay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (custname.isFocused()) {
                        Rect outRect = new Rect();
                        custname.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            custname.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (address.isFocused()) {
                        Rect outRect = new Rect();
                        address.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            address.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });
        spinnercustcat.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (mainlay.isFocused()) {
                        Rect outRect = new Rect();
                        mainlay.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            mainlay.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (custname.isFocused()) {
                        Rect outRect = new Rect();
                        custname.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            custname.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (address.isFocused()) {
                        Rect outRect = new Rect();
                        address.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            address.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });
        spinsubD.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (mainlay.isFocused()) {
                        Rect outRect = new Rect();
                        mainlay.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            mainlay.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (custname.isFocused()) {
                        Rect outRect = new Rect();
                        custname.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            custname.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (address.isFocused()) {
                        Rect outRect = new Rect();
                        address.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            address.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });
        spinvillage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (mainlay.isFocused()) {
                        Rect outRect = new Rect();
                        mainlay.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            mainlay.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (custname.isFocused()) {
                        Rect outRect = new Rect();
                        custname.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            custname.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (address.isFocused()) {
                        Rect outRect = new Rect();
                        address.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                            address.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });
    }

    private void intialisubds() throws JSONException {
        db = new DatabaseHandler(this);
        JSONArray Healthsubdarr = db.getHealthSudDetails();
        JSONArray Hygenesubdarr = db.getHygieneSudDetails();
        HealthsSubDs.add("SELECT");
        HygieneSubDs.add("SELECT");
        for (int i = 0; i < Healthsubdarr.length(); i++) {
            JSONObject obj = Healthsubdarr.optJSONObject(i);
            String code = obj.optString("subdcode", "");
            String name = obj.optString("subdname", "");
            HealthsSubDs.add(name + "-" + code);
        }
        for (int i = 0; i < Hygenesubdarr.length(); i++) {
            JSONObject obj = Hygenesubdarr.optJSONObject(i);
            String code = obj.optString("subdcode", "");
            String name = obj.optString("subdname", "");
            HygieneSubDs.add(name + "-" + code);
        }
    }

    private void initialisevillage(String id, String hid) throws JSONException {
        DatabaseHandler db = new DatabaseHandler(this);
        JSONArray vill1 = db.getvillagdetails(id);
        JSONArray vill2 = db.getvillagdetailsHygeneSubd(hid);
        if (vill1.length() == 0 && vill2.length() == 0) {
            Toast.makeText(getApplicationContext(), "No Villages Found under the SubDs!", Toast.LENGTH_LONG).show();
        }
        villages.clear();
        villages.add("SELECT");
        for (int i = 0; i < vill1.length(); i++) {
            JSONObject obj = vill1.optJSONObject(i);
            villages.add(obj.optString("villagename"));
        }
        for (int i = 0; i < vill2.length(); i++) {
            JSONObject obj = vill2.optJSONObject(i);
            if (!villages.contains(obj.optString("villagename"))) {
                villages.add(obj.optString("villagename"));
            }

        }
    }

    private void initvillagefromhealth(String id) throws JSONException {
        try {
            DatabaseHandler db = new DatabaseHandler(this);
            JSONArray vill1 = db.getvillagdetails(id);
            //JSONArray vill2 = db.getvillagdetailsHygeneSubd(hid);
            if (vill1.length() == 0) {
                Toast.makeText(getApplicationContext(), "No Villages Found under the SubDs!", Toast.LENGTH_LONG).show();
            }
            villages.clear();
            villages.add("SELECT");
            for (int i = 0; i < vill1.length(); i++) {
                JSONObject obj = vill1.optJSONObject(i);
                villages.add(obj.optString("villagename"));
            }
        }catch (Exception e){
            e.printStackTrace();
            Bugsnag.notify(e);
        }

    }

    private void initvillagefromhygene(String hid) throws JSONException {
        try{
            DatabaseHandler db = new DatabaseHandler(this);
            //JSONArray vill1 = db.getvillagdetails(id);
            JSONArray vill2 = db.getvillagdetailsHygeneSubd(hid);
            if (vill2.length() == 0) {
                Toast.makeText(getApplicationContext(), "No Villages Found under the SubDs!", Toast.LENGTH_LONG).show();
            }
            villages.clear();
            villages.add("SELECT");
            for (int i = 0; i < vill2.length(); i++) {
                JSONObject obj = vill2.optJSONObject(i);
                if (!villages.contains(obj.optString("villagename"))) {
                    villages.add(obj.optString("villagename"));
                }

            }
        }catch (Exception e){
            e.printStackTrace();
            Bugsnag.notify(e);
        }

    }

    private void initialisevillage() {
        villages.clear();
        villages.add("SELECT");
    }

    private void intialisecategories() throws JSONException {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("Config",
                Context.MODE_PRIVATE);
        String personrole = pref.getString(
                "personrole", "");
        db = new DatabaseHandler(getApplicationContext());
        JSONArray categoryarr = db.getcustomercategories(personrole);
        categories.add(getResources().getString(R.string.SELECT));
        for (int i = 0; i < categoryarr.length(); i++) {
            JSONObject obj = categoryarr.optJSONObject(i);
            if (!customercategorymap.containsKey(obj.optString("catname"))) {
                customercategorymap.put(obj.optString("catname"),
                        obj.optString("catcode"));
                categories.add(obj.optString("catname"));
            }
        }
    }

    private void verify() {
        View mView = NewCustomerActivity.this.getWindow().getDecorView();
        // getActivity().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        InputMethodManager imm = (InputMethodManager) mAct
                .getSystemService(Context.INPUT_METHOD_SERVICE);
//    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        EditText vertxt = ((EditText) mView
                .findViewById(R.id.ContactNumber));
        //vertxt.setFocusable(false);
        //ctpsn.setFocusable(false);
        mAct.getWindow()
                .setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // Button myBtn = (Button) mView.findViewById(R.id.verify);
        // myBtn.requestFocus();

        mobilenumber = ((EditText) mView
                .findViewById(R.id.ContactNumber)).getText().toString();
        // TODO Auto-generated method stub

        if (OTP_verificationstatus) {
            Global.Toast(mAct, "OTP already Verified",
                    Toast.LENGTH_SHORT, Font.Regular);
            return;
        }
        if (mobilenumber.length() < 10) {
            Global.Toast(mAct, "Enter a valid mobile number!",
                    Toast.LENGTH_SHORT, Font.Regular);
            return;
        }
        LayoutInflater inflater = getLayoutInflater();
        final View popupView1 = inflater.inflate(
                R.layout.enter_otp_option, null);
        final PopupWindow popupWindow1 = new PopupWindow(popupView1,
                WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow1.setTouchable(true);
        //popupWindow1.setFocusable(true);
        popupWindow1.setOutsideTouchable(false);
        popupWindow1
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        // popupWindow1.dismiss();
        popupWindow1.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow1.dismiss();
                    return true;
                }
                return false;
            }

        });
        LinearLayout l1 = (LinearLayout) mView
                .findViewById(R.id.linear1);
        popupWindow1.showAtLocation(l1, Gravity.CENTER, 0, 0);
        popupWindow1.setBackgroundDrawable(new ColorDrawable(
                Color.WHITE));
        // l1.clearFocus();
        l1.setFocusable(false);
        InputMethodManager immr = (InputMethodManager) mAct
                .getSystemService(Context.INPUT_METHOD_SERVICE);
//    immr.hideSoftInputFromWindow(mAct.getView().getWindowToken(), 0);
        ImageView close1 = (ImageView) popupView1
                .findViewById(R.id.close_window);
        Button call = (Button) popupView1.findViewById(R.id.call);
        Button sms = (Button) popupView1.findViewById(R.id.sms);
        // mTextField = (TextView)
        // popupView1.findViewById(R.id.message);
        TextView txtnum = (TextView) popupView1
                .findViewById(R.id.textView2);
        txtnum.setText("Verify " + mobilenumber);
        //txtnum.requestFocus();
        ((RelativeLayout) popupView1.findViewById(R.id.timer))
                .setVisibility(View.GONE);
//        popupWindow1.setCancelable(false);
        // /dialog.getContext().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (yourCountDownTimer != null) {
                    yourCountDownTimer.cancel();
                }
                popupWindow1.dismiss();
            }
        });
        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject cred = new JSONObject();
                try {
                    cred = GetOtprequest();
                    popupWindow1.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String[] params = new String[1];
                params[0] = cred.toString();
                if (isNetworkAvailable())
                    new OTPrequest(verification, mobilenumber, cred
                            .toString()).execute(params);
                else
                    Toast.makeText(mAct,
                            "Check Your Network Connection",
                            Toast.LENGTH_SHORT).show();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yourCountDownTimer != null) {
                    yourCountDownTimer.cancel();
                }
                //((ImageView) popupView1.findViewById(R.id.close_window))
                //	.setVisibility(View.GONE);
                ((Button) popupView1.findViewById(R.id.call))
                        .setEnabled(false);
                ((Button) popupView1.findViewById(R.id.sms))
                        .setEnabled(false);
                // popupWindow1.dismiss();
                ((RelativeLayout) popupView1.findViewById(R.id.timer))
                        .setVisibility(View.VISIBLE);
                mTextField = (TextView) popupView1
                        .findViewById(R.id.message);
                Calendar c = Calendar.getInstance();
                DateFormat dateTimeFormat = new SimpleDateFormat(
                        "yyyyMMddHHmmss");
                final String submittime = dateTimeFormat.format(c
                        .getTime());
                // boolean verify=false;
                yourCountDownTimer = new CountDownTimer(60000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        mTextField.setText("" + millisUntilFinished
                                / 1000);
                        if (((millisUntilFinished / 1000) % 3) == 0)
                            if (getCallDetails(mAct, mobilenumber,
                                    popupWindow1, submittime)) {
                                verify = true;
                                onFinish();
                            }
                    }

                    @Override
                    public void onFinish() {
                        if (yourCountDownTimer != null) {
                            yourCountDownTimer.cancel();
                        }
                        // TODO Auto-generated method stub
                        mTextField.setText("0");
                        ((RelativeLayout) popupView1
                                .findViewById(R.id.timer))
                                .setVisibility(View.GONE);
                        ((Button) popupView1.findViewById(R.id.call))
                                .setEnabled(true);
                        ((Button) popupView1.findViewById(R.id.sms))
                                .setEnabled(true);
                        if (verify) {
                            Global.Toast(mAct, "Mobile Number Verified",
                                    Toast.LENGTH_SHORT, Font.Regular);
                        } else {
                            Global.Toast(mAct,
                                    "Mobile Number Verification Failed",
                                    Toast.LENGTH_SHORT, Font.Regular);
                        }
                    }
                }.start();
            }
        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private File createTemporaryFile(String part, String ext) throws Exception {

        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public void next(View v) {
        try{
            String subdval = "";
            String hygenesubdval = "";
            String custname = ((TextView) findViewById(R.id.custname)).getText().toString();
            String address = ((TextView) findViewById(R.id.address)).getText().toString();
            String phone = ((TextView) findViewById(R.id.ContactNumber)).getText().toString();
            String pin = ((TextView) findViewById(R.id.pin)).getText().toString();
            spinner = findViewById(R.id.spinnercustcat);
            String category = spinner.getSelectedItem().toString();

            Spinner sd = findViewById(R.id.spinsubD);
            String val1 = sd.getSelectedItem().toString();

            if (!(val1.contentEquals("SELECT") || val1.contentEquals(""))) {
                String[] parts = val1.split("-");
                String name = parts[0];
                if (parts.length>1){
                    String code = parts[1];
                    if (name != "") {
                        subdval = db.getsubdid(name, code);
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Please add SubD Code and Try Again",Toast.LENGTH_LONG).show();
                }
            }


            Spinner hsd = findViewById(R.id.hygenesubD);
            String hval1 = hsd.getSelectedItem().toString();

            if (!(hval1.contentEquals("SELECT") || hval1.contentEquals(""))) {
                String[] parts1 = hval1.split("-");
                String name1 = parts1[0];
                if (parts1.length>1){
                    String code1 = parts1[1];
                    if (name1 != "") {
                        hygenesubdval = db.getsubdid(name1, code1);
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Please add SubD Code and Try Again",Toast.LENGTH_LONG).show();
                }


            }

            String val2 = "";
            String villageval = "";
            try {
                villagespin = findViewById(R.id.spinvillage);
                val2 = villagespin.getSelectedItem().toString();
                villageval = db.getvillageid(val2);
            } catch (Exception e) {
                EFlag = 1;
                val2 = "";
                villageval = "";
                String isvalid = valid(custname, address, phone, pin, category, val1, hval1, val2);
                Log.e("isvalid", isvalid);
                if (!isvalid.contentEquals("TRUE")) {
                    Toast.makeText(NewCustomerActivity.this, isvalid, Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(NewCustomerActivity.this, SurrogatesActivity.class);
                intent.putExtra("custname", custname);
                intent.putExtra("address", address);
                intent.putExtra("villageval", villageval);
                intent.putExtra("subdval", subdval);
                intent.putExtra("hygenesubdval", hygenesubdval);
                intent.putExtra("phone", phone);
                intent.putExtra("pin", pin);
                intent.putExtra("category", category);
                intent.putExtra("OTP_verificationstatus", OTP_verificationstatus);
                if (OTP_verificationstatus)
                    intent.putExtra("OTP_submitTime", OTP_submitTime);
                Global.sharedcustimg = customerphoto;
                Global.sharedinsideimg = insidephoto;
                finish();
                startActivity(intent);
                overridePendingTransition(0, 0);
            }

            if (EFlag != 1) {
                String isvalid = valid(custname, address, phone, pin, category, val1, hval1, val2);
                Log.e("isvalid", isvalid);
                if (!isvalid.contentEquals("TRUE")) {
                    Toast.makeText(NewCustomerActivity.this, isvalid, Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(NewCustomerActivity.this, SurrogatesActivity.class);
                intent.putExtra("custname", custname);
                intent.putExtra("address", address);
                intent.putExtra("villageval", villageval);
                intent.putExtra("subdval", subdval);
                intent.putExtra("hygenesubdval", hygenesubdval);
                intent.putExtra("phone", phone);
                intent.putExtra("pin", pin);
                intent.putExtra("category", category);
                intent.putExtra("OTP_verificationstatus", OTP_verificationstatus);
                if (OTP_verificationstatus)
                    intent.putExtra("OTP_submitTime", OTP_submitTime);
                Global.sharedcustimg = customerphoto;
                Global.sharedinsideimg = insidephoto;
                finish();
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        }catch (Exception e){
            e.printStackTrace();
            Bugsnag.notify(e);
        }
    }

    private String valid(String custname, String address, String phone, String pin, String category, String HealthSubD, String HygeneSubD, String village) {
        Log.e("cateogry", category);
        String temp = "TRUE";
        if (custname.contentEquals(""))
            return getResources().getString(R.string.name_required);
        if (address.contentEquals(""))
            return getResources().getString(R.string.addr_required);
        if (HealthSubD.contentEquals("SELECT") || HealthSubD.contentEquals(""))
            return "Please Choose a Health SubD";
        if (HygeneSubD.contentEquals("SELECT") || HygeneSubD.contentEquals(""))
            return "Please Choose a Hygene SubD";
        if (village.contentEquals("SELECT") || village.contentEquals(""))
            return "Please Choose a Village";
//        if (phone.contentEquals(""))
//            return getResources().getString(R.string.number_required);
        // if (phone.length() < 10)
        //  return getResources().getString(R.string.number_notvalid);
//        if (pin.contentEquals(""))
//            return getResources().getString(R.string.pin_required);
        if (!(HealthSubD.contentEquals("SELECT") || HealthSubD.contentEquals(""))) {
            String[] parts = HealthSubD.split("-");
            if (parts.length>1){
                String code = parts[1];
            }else{
                return "Please add Code for Health SubD and Try Again";
            }
        }
        if (!(HygeneSubD.contentEquals("SELECT") || HygeneSubD.contentEquals(""))) {
            String[] parts = HygeneSubD.split("-");
            if (parts.length>1){
                String code = parts[1];
            }else{
                return "Please add Code for Hygiene SUbD and Try Again";
            }
        }
        if (customerphoto == null)
            return getResources().getString(R.string.photo_required);
        if (insidephoto == null)
            return getResources().getString(R.string.inside_required);
        if (category == null || category.contentEquals("SELECT"))
            return "Customer Category Required!";

        return temp;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap mImageBitmap = (Bitmap) extras.get("data");
                ImageView proof;
                ImageView inside;


                if (type == "photo") {
                    customerphoto = getResizedBitmap(mImageBitmap, 500, 500);
                    proof = (ImageView) findViewById(R.id.addcust);
                    proof.setImageBitmap(customerphoto);
                } else if (type == "inside") {
                    insidephoto = getResizedBitmap(mImageBitmap, 500, 500);
                    inside = (ImageView) findViewById(R.id.inside);
                    inside.setImageBitmap(insidephoto);
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.turn_off_high_quality_img, Toast.LENGTH_SHORT).show();
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    @Override
    public void onBackPressed() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.linear1);
        LayoutInflater layoutInflater = (LayoutInflater) NewCustomerActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popuplayout, null);
        TextView header = (TextView) customView.findViewById(R.id.header);
        header.setText(R.string.savecustomer);
        TextView msg = (TextView) customView.findViewById(R.id.message);
        msg.setText(R.string.savecustomermsg);
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                //super.onBackPressed();
                Intent intent = new Intent(NewCustomerActivity.this, LandingActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    public JSONObject GetOtprequest() throws JSONException {
        webconfigurration C = new webconfigurration(getApplicationContext());
        String params = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"OtpGeneration\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject cred = new JSONObject(params);
        try {
            JSONObject updatestatus = getstatusobject();
            cred.put("updatestatus", updatestatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject data = new JSONObject();
        data.put("operation", "verifyOtp").put("Operation", "verifyOtp")
                .put("MobileNumber", mobilenumber).put("custid", "");
        cred.getJSONObject("data").getJSONArray("data").put(data);

        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        cred.put("auth", auth);

        return cred;
    }

    private JSONObject getstatusobject() throws Exception {
        JSONObject temp = new JSONObject();
        SharedPreferences pref = getSharedPreferences("Config",
                Context.MODE_PRIVATE);

        String person = "";
        String lat = "";
        String lng = "";
        String operation = "";
        String version = "";
        String createdon = "";
        String uuid = "";
        String appname = "";
        String versioncode = "";
        person = pref.getString("personid", "0");

        operation = webconfigurration.status;
        appname = webconfigurration.appname;
        lat = webconfigurration.lat;
        lng = webconfigurration.lng;

        PackageInfo pInfo = getPackageManager().getPackageInfo(
                getPackageName(), 0);
        version = pInfo.versionName;
        versioncode = String.valueOf(pInfo.versionCode);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        createdon = df.format(c.getTime());

        uuid = UUID.randomUUID().toString();

        temp.put("person", person).put("lat", lat).put("lng", lng)
                .put("operation", operation).put("version", version)
                .put("versioncode", versioncode).put("appname", appname)
                .put("createdon", createdon).put("uuid", uuid);
        return temp;
    }

    public void Intialise_OTP_popup(View v, String mobilenumber_in,
                                    String Requeststring_in, boolean send_status, String otp) {

        final String otp_gen = otp;
        final String mobilenumber = mobilenumber_in;
        final String Requeststring = Requeststring_in;

        AlertDialog.Builder builder = new AlertDialog.Builder(mAct);
        LayoutInflater inflater = getLayoutInflater();
        final View popupView = inflater.inflate(R.layout.enter_otp, null);
        builder.setView(popupView);
        final AlertDialog popupWindow = builder.create();
        popupWindow.show();
        popupWindow.setCanceledOnTouchOutside(false);
        //final View popupView = inflater.inflate(R.layout.enter_otp, null);
        //final PopupWindow popupWindow = new PopupWindow(popupView,
        //	LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        //popupWindow.setTouchable(true);
        //popupWindow.setFocusable(true);
        //popupWindow.setOutsideTouchable(true);
        //popupWindow
        //		.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        //popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);

        ImageView close = (ImageView) popupView.findViewById(R.id.close_window);

        final TextView error = (TextView) popupView
                .findViewById(R.id.textview4);
        error.setVisibility(View.GONE);

        ImageView resend = (ImageView) popupView.findViewById(R.id.Resend);
        final TextView staus_message = (TextView) popupView
                .findViewById(R.id.textView2);

        final EditText code = (EditText) popupView.findViewById(R.id.otpvalue);

        Button confirm = (Button) popupView.findViewById(R.id.confirm);

        Button cancel = (Button) popupView.findViewById(R.id.cancel);
        resend.setVisibility(View.VISIBLE);
        ((TextView) popupView.findViewById(R.id.textview6))
                .setVisibility(View.VISIBLE);
        if (!send_status) {
            staus_message.setText("*Cannot send OTP to " + mobilenumber
                    + "..Try Resend");
            staus_message.setTextColor(Color.parseColor("#FF0000"));
            confirm.setEnabled(false);

        } else {
            staus_message.setText("OTP Send to " + mobilenumber);
            staus_message.setTextColor(Color.parseColor("#008000"));
            confirm.setEnabled(true);
        }

        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindow.dismiss();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {

            private String verificationcode_entered;

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String code_value = code.getText().toString();
                if (otp_gen.equals(code_value) && !code_value.equals("")) {
                    // Toast.makeText(mContext, "codematched",
                    // Toast.LENGTH_SHORT).show();
                    error.setVisibility(View.VISIBLE);
                    error.setText("Code Matched");
                    error.setTextColor(Color.parseColor("#008000"));
                    popupWindow.dismiss();
                    OTP_verificationstatus = true;
                    verificationcode_entered = code_value;
                    contactnumber.setEnabled(false);
                    verification.setText("VERIFIED");
                    tick.setVisibility(View.VISIBLE);
//                    if (OTP_verificationstatus) {
//                        //ImageView tickbutton = (ImageView) mView.findViewById(R.id.tickbutton);
//                        tick.setVisibility(View.VISIBLE);
//                    }

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat(
                            "dd/MM/yyyy HH:mm:ss");
                    String current = sdf.format(c.getTime());
                    OTP_submitTime = current;

                } else {
                    error.setText("* Error Code");
                    error.setTextColor(Color.parseColor("#FF0000"));
                    error.setVisibility(View.VISIBLE);

                }
            }
        });

        resend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String[] cred = new String[1];
                cred[0] = Requeststring;

                if (isNetworkAvailable()) {

                    Toast.makeText(mAct, "Resending code", Toast.LENGTH_SHORT)
                            .show();
                    popupWindow.dismiss();
                    new OTPrequest(v, mobilenumber, Requeststring)
                            .execute(cred);
                } else {
                    error.setVisibility(View.VISIBLE);
                    error.setText("* You are offline");
                }
            }

        });

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindow.dismiss();

            }
        });

    }

    // ----------------------Number Call Verification
    // --------------------
    private Boolean getCallDetails(Context context,
                                   String mobilenumber1, PopupWindow popupWindow1,
                                   String submittime) {
        StringBuffer stringBuffer = new StringBuffer();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {

            return false;
        }
        Cursor cursor = getContentResolver().query(
                CallLog.Calls.CONTENT_URI, null, null, null,
                CallLog.Calls.DATE + " DESC");
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);
        Boolean verified = false;
        int count = 0;
        while (cursor.moveToNext()) {
            if (count < 3) {

                String phNumber = cursor.getString(number);
                String callType = cursor.getString(type);
                String callDate = cursor.getString(date);
                Date callDayTime = new Date(Long.valueOf(callDate));
                String callDuration = cursor.getString(duration);
                String dir = null;
                long dateTimeMillis = cursor.getLong(cursor
                        .getColumnIndex(CallLog.Calls.DATE));
                Date datetime = new Date(dateTimeMillis);
                String dateString = new SimpleDateFormat(
                        "yyyyMMddHHmmss").format(datetime);
                double intdateString = Double.parseDouble(dateString);
                double intsubmittime = Double.parseDouble(submittime);
                if (intdateString > intsubmittime) {
                    int len = phNumber.length();
                    phNumber = phNumber.substring(len - 10, len);
                    int dircode = Integer.parseInt(callType);
                    switch (dircode) {
                        case 1:
                            if (mobilenumber.contentEquals(phNumber)) {
                                popupWindow1.dismiss();
                                OTP_verificationstatus = true;
                                // verificationcode_entered = code_value;
                                contactnumber.setEnabled(false);
                                verification.setText("VERIFIED");
                                if (OTP_verificationstatus) {
                                    ImageView tickbutton = (ImageView) findViewById(R.id.tickbutton);
                                    tickbutton.setVisibility(View.VISIBLE);
                                }
                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "dd/MM/yyyy HH:mm:ss");
                                String current = sdf.format(c.getTime());
                                OTP_submitTime = current;
                                verified = true;
                            }
                            break;
                        case 2:// CallLog.Calls.INCOMING_TYPE: dir =
                            // "INCOMING";
                            // Toast.makeText(context,
                            // "OUTGOING call Number=="+phNumber
                            // +" Type=="+callType + "date=="
                            // +callDayTime+
                            // "duration=="+callDuration,
                            // Toast.LENGTH_SHORT).show();
                            break;
                        case 3:
                            if (mobilenumber.contentEquals(phNumber)) {

                                // Toast.makeText(context,
                                // "Equal MISSED call Number=="+phNumber,
                                // Toast.LENGTH_SHORT).show();
                                popupWindow1.dismiss();
                                OTP_verificationstatus = true;
                                // verificationcode_entered = code_value;
                                contactnumber.setEnabled(false);
                                verification.setText("VERIFIED");
                                if (OTP_verificationstatus) {
                                    ImageView tickbutton = (ImageView) findViewById(R.id.tickbutton);
                                    tickbutton.setVisibility(View.VISIBLE);
                                }
                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "dd/MM/yyyy HH:mm:ss");
                                String current = sdf.format(c.getTime());
                                OTP_submitTime = current;
                                verified = true;

                            }
                            break;

                        case 5:
                            if (mobilenumber.contentEquals(phNumber)) {

                                // Toast.makeText(context,
                                // "Equal MISSED call Number=="+phNumber,
                                // Toast.LENGTH_SHORT).show();
                                popupWindow1.dismiss();
                                OTP_verificationstatus = true;
                                // verificationcode_entered = code_value;
                                contactnumber.setEnabled(false);
                                verification.setText("VERIFIED");
                                if (OTP_verificationstatus) {
                                    ImageView tickbutton = (ImageView) findViewById(R.id.tickbutton);
                                    tickbutton.setVisibility(View.VISIBLE);
                                }
                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "dd/MM/yyyy HH:mm:ss");
                                String current = sdf.format(c.getTime());
                                OTP_submitTime = current;
                                verified = true;

                            }
                            break;
                    }
                    count++;
                }
            }

        }
        cursor.close();
        return verified;// .toString();
    }

    // otp request operation
    public class OTPrequest extends AsyncTask<String, Integer, String> {
        final Dialog innerdialog = new Dialog(mAct);
        private View v;
        private String mobilenumber;
        private String Requeststring;

        public OTPrequest(View v, String mobilenumber, String Requeststring) {
            this.v = v;
            this.mobilenumber = mobilenumber;
            this.Requeststring = Requeststring;
        }

        @Override
        protected void onPreExecute() {
            innerdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            innerdialog.getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            innerdialog.setContentView(R.layout.adapter_progressdialog);
            TextView txt = (TextView) innerdialog.findViewById(R.id.text);
            txt.setText("Please wait..");
            innerdialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            innerdialog.show();
            innerdialog.setCancelable(false);

        }

        @Override
        protected String doInBackground(String... credentials) {

            boolean result = false;
            int trycount = 0;
            while (!result && trycount < 2) {
                trycount++;
                try {

                    String targetURL = webconfigurration.url;
                    URL connUrl = new URL(targetURL);
                    HttpURLConnection conn = (HttpURLConnection) connUrl
                            .openConnection();
                    conn.setRequestProperty("Content-Type",
                            "application/json; charset=utf-16");
                    conn.setRequestProperty("Accept-Encoding", "identity");

                    conn.setConnectTimeout(10000);
                    conn.setUseCaches(false);
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setReadTimeout(10000);
                    conn.setRequestMethod("POST");

                    int totalLength = (credentials[0].toString().getBytes(
                            "utf-16").length);
                    conn.setFixedLengthStreamingMode(totalLength);
                    DataOutputStream request = new DataOutputStream(
                            conn.getOutputStream());
                    request.write(credentials[0].toString().getBytes("utf-16"));
                    request.flush();
                    request.close();

                    InputStream is = conn.getInputStream();

                    BufferedReader rd = new BufferedReader(
                            new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while ((line = rd.readLine()) != null) {
                        response.append(line);
                    }
                    final String responsefrom = response.toString();
                    rd.close();
                    result = true;
                    return responsefrom;
                } catch (Exception e) {
                    result = false;
                    e.printStackTrace();
                }
            }
            return null;
        }

        protected void onPostExecute(String result) {
            innerdialog.dismiss();

            // validating status of sms send
            String otp = "";
            boolean send_status = true;
            if (result == null) {
                send_status = false;

            } else {

                try {
                    JSONObject resp = new JSONObject(result);
                    String status = resp.getString("status");
                    otp = resp.getString("otp");
                    if (otp.equals("") || !status.equalsIgnoreCase("success"))
                        send_status = false;
                } catch (JSONException e) {
                    send_status = false;
                }
            }

            Intialise_OTP_popup(v, mobilenumber, Requeststring, send_status,
                    otp);
        }

    }
}
