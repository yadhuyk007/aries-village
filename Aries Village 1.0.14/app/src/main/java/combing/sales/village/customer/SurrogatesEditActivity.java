package combing.sales.village.customer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

import combing.sales.village.ariesc.R;
import combing.sales.village.database.DatabaseHandler;
import combing.sales.village.gps.GPSAccuracy;
import combing.sales.village.gps.PlayTracker;
import combing.sales.village.landing.LandingActivity;
import combing.sales.village.objects.Font;
import combing.sales.village.objects.Global;
import combing.sales.village.objects.Locations;
import combing.sales.village.objects.Scheduledetail;
import combing.sales.village.objects.Stacklogger;

public class SurrogatesEditActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_GPS_ACCURACY = 1;
    public static String[] productcategories;
    public static boolean[] selectedarray;
    public static String[] categorycode;
    public static String[] typearray;
    public static String[] selectedarraytext;
    private String pin;
    private String custcategory;
    private String phone;
    private String address;
    private String custname;
    private String custid;
    private String scheduledetailid;
    private DatabaseHandler db;
    private SharedPreferences pref;
    private String customerid = "";
    private String createtime = "";
    private Bitmap customerphoto;
    private Bitmap insidephoto;
    private boolean OTP_verificationstatus;
    private String OTP_submitTime;
    private String villageval;
    private String subdval;
    private String hygenesubdval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surrogates_edit);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));

        db = new DatabaseHandler(getApplicationContext());
        pref = getApplicationContext()
                .getSharedPreferences("Config",
                        MODE_PRIVATE);

        Bundle bundle = getIntent().getExtras();
        custname = bundle.getString("custname", "");
        address = bundle.getString("address", "");
        phone = bundle.getString("phone", "");
        pin = bundle.getString("pin", "");
        custcategory = bundle.getString("category","");
        scheduledetailid= bundle.getString("scheduledetailid","");
        OTP_verificationstatus = bundle.getBoolean("OTP_verificationstatus", false);
        OTP_submitTime = bundle.getString("OTP_submitTime", "");
        customerphoto = Global.sharedcustimg;
        insidephoto = Global.sharedinsideimg;
        villageval = bundle.getString("villageval","");
        subdval = bundle.getString("subdval","");
        hygenesubdval = bundle.getString("hygenesubdval","");
        ((Button) findViewById(R.id.next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

        try {
            intialisedialoguedata();
        } catch (Exception e) {
            e.printStackTrace();
        }
        custid= db.getcustomerid(scheduledetailid);

        LinearLayout group = (LinearLayout)findViewById(R.id.outlinear);

        TextView customTitleView = new TextView(SurrogatesEditActivity.this);
        customTitleView.setText("SURROGATES");
        customTitleView.setTextSize(20);
        customTitleView.setPadding(20, 20, 20, 20);
        customTitleView.setBackgroundResource(R.color.gray2);
//        Font a = new Font(mAct, customTitleView, Font.Semibold);
//        builder.setCustomTitle(customTitleView);
        for (int ct = 0; ct < productcategories.length; ct++) {
            JSONObject checklist = null;
            try {
                checklist = db.getchecklists(scheduledetailid, categorycode[ct]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (checklist.length()>0) {
                final int which = ct;
                if (typearray[ct] == null
                        || typearray[ct].contentEquals("")) {
                    LayoutInflater inflater = getLayoutInflater();
                    View child = inflater.inflate(R.layout.surrogate_checkbox, null);
                    //final TextView first = new TextView(SurrogatesActivity.this);
                    final TextView first = (TextView) child.findViewById(R.id.txt);
                    first.setText(productcategories[ct]);
                    //group.addView(first);
                    //CheckBox firstCheck = new CheckBox(SurrogatesActivity.this);
                    CheckBox firstCheck = (CheckBox) child.findViewById(R.id.chkbox);
                   // if (selectedarray[ct])
                        firstCheck.setChecked(true);
                    selectedarray[which] = true;

                    // firstCheck.setGravity(Gravity.RIGHT);

                    group.addView(child);
                    // group.addView(firstCheck);
                    firstCheck
                            .setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    CheckBox c = (CheckBox) v;
                                    if (c.isChecked()) {
                                        // Toast.makeText(getActivity(),
                                        // "True", Toast.LENGTH_LONG)
                                        // .show();
                                        selectedarray[which] = true;
                                    } else {
                                        selectedarray[which] = false;
                                    }
                                }
                            });

                }
                else {
                    LayoutInflater inflater = getLayoutInflater();
                    View child = inflater.inflate(R.layout.surrogate_inputbox, null);
                    //final TextView first = new TextView(SurrogatesActivity.this);
                    final TextView first = (TextView) child.findViewById(R.id.txt);
                    first.setText(productcategories[ct]);

                    //group.addView(first);
                    //final EditText firstEdit = new EditText(SurrogatesActivity.this);
                    EditText firstEdit = (EditText) child.findViewById(R.id.chkbox);
                    try {
                        firstEdit.setText(checklist.getString("input"));
                        if(checklist.getString("input") != "") {
                            selectedarray[which] = true;
                            selectedarraytext[which] = checklist.getString("input");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (typearray[ct].equalsIgnoreCase("Number"))
                        firstEdit.setInputType(InputType.TYPE_CLASS_PHONE);
                    //group.addView(firstEdit);
                    group.addView(child);
                    firstEdit.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void onTextChanged(CharSequence s,
                                                  int start, int before, int count) {
                            // Toast.makeText(getActivity(), s + "",
                            // Toast.LENGTH_LONG).show();
                            selectedarray[which] = true;
                            selectedarraytext[which] = s + "";
                            if(selectedarraytext[which] == "")
                            {

                                selectedarray[which] = false;
                            }
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s,
                                                      int start, int count, int after) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            // TODO Auto-generated method stub
                        }
                    });
                }

            } else
            {
                final int which = ct;
            if (typearray[ct] == null
                    || typearray[ct].contentEquals("")) {
                LayoutInflater inflater = getLayoutInflater();
                View child = inflater.inflate(R.layout.surrogate_checkbox, null);
                //final TextView first = new TextView(SurrogatesActivity.this);
                final TextView first = (TextView) child.findViewById(R.id.txt);
                first.setText(productcategories[ct]);
                //group.addView(first);
                //CheckBox firstCheck = new CheckBox(SurrogatesActivity.this);
                CheckBox firstCheck = (CheckBox) child.findViewById(R.id.chkbox);
                if (selectedarray[ct])
                    firstCheck.setChecked(true);

                // firstCheck.setGravity(Gravity.RIGHT);

                group.addView(child);
                // group.addView(firstCheck);
                firstCheck
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                CheckBox c = (CheckBox) v;
                                if (c.isChecked()) {
                                    // Toast.makeText(getActivity(),
                                    // "True", Toast.LENGTH_LONG)
                                    // .show();
                                    selectedarray[which] = true;
                                } else {
                                    selectedarray[which] = false;
                                }
                            }
                        });

            } else {
                LayoutInflater inflater = getLayoutInflater();
                View child = inflater.inflate(R.layout.surrogate_inputbox, null);
                //final TextView first = new TextView(SurrogatesActivity.this);
                final TextView first = (TextView) child.findViewById(R.id.txt);
                first.setText(productcategories[ct]);

                //group.addView(first);
                //final EditText firstEdit = new EditText(SurrogatesActivity.this);
                EditText firstEdit = (EditText) child.findViewById(R.id.chkbox);
                firstEdit.setText(selectedarraytext[ct]);
                if (typearray[ct].equalsIgnoreCase("Number"))
                    firstEdit.setInputType(InputType.TYPE_CLASS_PHONE);
                //group.addView(firstEdit);
                group.addView(child);
                firstEdit.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s,
                                              int start, int before, int count) {
                        // Toast.makeText(getActivity(), s + "",
                        // Toast.LENGTH_LONG).show();
                        selectedarray[which] = true;
                        selectedarraytext[which] = s + "";
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s,
                                                  int start, int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        // TODO Auto-generated method stub
                    }
                });
            }
        }
        }
//        // *****************
//
//        builder.setPositiveButton("CLOSE",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog,
//                                        int which) {
//                        dialog.dismiss();
//                    }
//                });
//        final AlertDialog popupWindow = builder.create();
//        popupWindow.show();


    }

    public void next() {
        turnOnGPS();
        if (!PlayTracker.intime()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SurrogatesEditActivity.this);
            builder.setTitle(R.string.gps_unavailable);
            builder.setMessage(R.string.check_your_gps);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

            AlertDialog dialog = builder.create();
            builder.show();

            return;
        }
        if (locationaccuracy() != 3) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SurrogatesEditActivity.this);
            builder.setTitle(R.string.enable_high_loc_accuracy);
            builder.setMessage(R.string.loc_acc_low);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    });

            builder.show();

            return;
        }

        Intent intent = new Intent(SurrogatesEditActivity.this,
                GPSAccuracy.class);
        intent.putExtra("operation", "editlocation");
        startActivityForResult(intent, REQUEST_CODE_GPS_ACCURACY);
        overridePendingTransition(0, 0);
    }

    private int locationaccuracy() {
        int locationMode = -1;
        try {
            locationMode = Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode;
    }

    private void turnOnGPS() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!statusOfGPS) {

            AlertDialog.Builder builder = new AlertDialog.Builder(SurrogatesEditActivity.this);
            builder.setTitle(R.string.switch_on_gps);
            builder.setMessage(R.string.gps_off_alert_message);


            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            startActivityForResult(
                                    new Intent(
                                            Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });


            builder.show();

            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // ****************GPS Changes**********************
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Double lati = data.getDoubleExtra("latit", 0);
                Double longi = data.getDoubleExtra("longit", 0);
                Float acc = data.getFloatExtra("minacc", 0);
                String provi = data.getStringExtra("prov");
                String operation = data.getStringExtra("operation");
                // Toast.makeText(getApplicationContext(),
                // lati + ", " + longi + ", " + provi + ", " + acc,
                // Toast.LENGTH_LONG).show();
                // gowithcurrent(lati, longi, acc, provi);
                if (lati > 0 && longi > 0)
                    saveData(lati, longi, acc, provi);
                else
                    Global.Toast(SurrogatesEditActivity.this,
                            "Location not found. Please try again!",
                            Toast.LENGTH_LONG, Font.Regular);
            }
            if (locationaccuracy() != 3) {
                AlertDialog.Builder builder = Global.Alert(SurrogatesEditActivity.this,
                        "Enable High Location Accuracy", "Currently location accuracy low.");
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                Global.setDialogFont(SurrogatesEditActivity.this, dialog);
                return;
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Global.Toast(SurrogatesEditActivity.this, "Cancelled!",
                        Toast.LENGTH_LONG, Font.Regular);
                // Write your code if there's no result
            }
        }

    }

    public void saveData(double lati, double longi, float acc, String provi) {
        if (lati > 0 && longi > 0) {
            String schbegintime = db.getschedulebegintime();
            if (schbegintime == null || schbegintime == "") {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.SECOND, (c.get(Calendar.SECOND) - 10));
                DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                String sync = dateTimeFormat.format(c.getTime());
                //db.intialisesynctable(sync);

                c = Calendar.getInstance();
                c.set(Calendar.SECOND, (c.get(Calendar.SECOND) - 5));
                schbegintime = dateTimeFormat.format(c.getTime());
                //db.updatescheduleheaderbegintime(schbegintime,sync);

                SharedPreferences pref = getApplicationContext().getSharedPreferences(
                        "Config", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("Schbegintime", schbegintime);
                editor.commit();
                Log.e("Schedulebegins", schbegintime);
            }
            try {
                String uuid = UUID.randomUUID().toString();
                String name = custname;
                String latitude = lati + "";
                String longitude = longi + "";
                String custcode = "";
                String category = custcategory;
                String gstin = "";
                String landmark = "";
                String contactperson = "";
                String contactnumber = phone;
                String othercompanies = "";
                String town = "";
                String categoryids = "";
                String pan = hygenesubdval;
                String email = villageval;
                boolean otpverified = OTP_verificationstatus;
                String locprovider = provi+"";
                String locaccuracy = acc+"";
                String directcov = "";
                String closingday = "";
                String storeclosed = "";
                boolean checkedpng = false;
                String branch = "";
                String drugLicienceNo = "";
                String locality = "";
                String city =  "";
                String state = "";
                String coverageDay = "";
                String week1 = "";
                String week2 = "";
                String week3 = "";
                String week4 = "";
                String visitFrequency = "";
                String type = "";
                String wholeSale = "";
                String metro = "";
                String classification = "";
                String latitudePhoto = "";
                String longitudePhoto = "";
                String remarks = "";
                String marketname = subdval;
                String startTime = "";
                String endTime = "";
                String otpsubtime = OTP_submitTime;

                String productcats = "";
                String selectedproductcategories = "";
                for (int i = 0; i < selectedarray.length; i++) {
                    if (selectedarray[i]) {
                        selectedproductcategories += categorycode[i] + ",";
                    }
                }
                 if (!selectedproductcategories.equals(""))
                    productcats = selectedproductcategories;

                Editlocation(name, address, latitude, longitude, custcode, category,
                        gstin, landmark, contactperson, contactnumber,
                        othercompanies, town, productcats, categoryids, pan, email,
                        otpverified ? "1" : "0", locprovider, locaccuracy, uuid,
                        directcov, closingday, String.valueOf(storeclosed),
                        checkedpng ? "1" : "0", branch, drugLicienceNo, locality,
                        city, state, pin, coverageDay, week1, week2, week3, week4,
                        visitFrequency, type, wholeSale, metro, classification,
                        latitudePhoto, longitudePhoto, schbegintime, remarks,
                        marketname, selectedarraytext, categorycode,
                        startTime, endTime, otpsubtime);
                customerid = db.getcustomerid(scheduledetailid);
                Calendar c = Calendar.getInstance();
                DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                String crtime = dateTimeFormat.format(c.getTime());
                DateFormat dateTimeFormat_payment = new SimpleDateFormat(
                        "dd/MM/yyyy HH:mm:ss");
                String edittime = dateTimeFormat_payment.format(c.getTime());
                if (customerphoto != null) {
                    String filename = uuid + ".png";
                    db.editCustomerPhoto(scheduledetailid, uuid, crtime,
                            lati+"",longi+"",acc+"", edittime);


                    createDirectoryAndsavetempfile(customerphoto, "photo_"
                            + filename);
                }
                if(otpverified){
                    db.otplatlongupdate(scheduledetailid, crtime,
                            lati+"",longi+"");

                }
                String uuid1 = UUID.randomUUID().toString();
                HashMap<String, Bitmap> photos = new HashMap<String, Bitmap>();
                photos.put(uuid1, insidephoto);
                if (photos != null && !customerid.equals("") && photos.size() > 0) {
                    db.insertDocumentPhotos(scheduledetailid, photos, crtime, "1");
                }
                if (photos != null && !customerid.equals("") && photos.size() > 0) {
                    for (String key : photos.keySet()) {
                        try {
                            Bitmap bitmap = photos.get(key);
                            String filename = key + ".png";
                            createDirectoryAndsavetempfile(bitmap, "document_"
                                    + filename);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(SurrogatesEditActivity.this, R.string.please_try_again, Toast.LENGTH_SHORT).show();

                try {
                    File tempDir = Environment.getExternalStorageDirectory();
                    tempDir = new File(tempDir.getAbsolutePath() + "/AriesErrors/");
                    if (!tempDir.exists()) {
                        tempDir.mkdirs();
                    }
                    Calendar c = Calendar.getInstance();
                    String myFormat = "dd-MM-yyyy HH:mm:ss";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    myFormat = "yyyyMMddHHmmss";
                    sdf = new SimpleDateFormat(myFormat, Locale.US);
                    String filedate = sdf.format(c.getTime());
                    File myExternalFile = new File(tempDir, "errorlog_" + filedate
                            + ".txt");
                    String error = filedate + "-----------";
                    error += e.toString();
                    FileOutputStream fos = new FileOutputStream(myExternalFile);
                    OutputStreamWriter osw = new OutputStreamWriter(fos);
                    osw.append(error);
                    osw.close();
                } catch (IOException ioe) {
                    // ...
                }
            }finally {
                finishActivityFromHere();
            }
        }
    }

    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void finishActivityFromHere() {
        Intent intent = new Intent(SurrogatesEditActivity.this, LandingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }


    public void Editlocation(String name, String adress, String latitude,
                            String longitude, String costomercode, String customercategory,
                            String tinnumber, String nearestlandmarkvalue,
                            String contactpersonvalue, String Contactpersonnumbervalue,
                            String anyothercompanyvalue, String townclassvalue,
                            String selectedproductcategories, String customercatogorycode,
                            String pan, String email, String otpverified, String locprovider,
                            String locaccuracy, String uuid, String directcov,
                            String closingday, String storeclosed, String pngcovered,
                            String branch, String drugLicienceNo, String locality, String city,
                            String state, String pin, String coverageDay, String week1,
                            String week2, String week3, String week4, String visitFrequency,
                            String type, String wholeSale, String metro, String classification,
                            String latitudePhoto, String longitudePhoto, String schbegintime,
                            String remarks, String marketname, String[] selectedarraytext,
                            String[] categorycodearray, String startTime, String endTime, String otpsubtime)
            throws Exception {
        Locations loctodb = new Locations();
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String crtime = dateTimeFormat.format(c.getTime());
        Log.e("CreatedCust", crtime);


        String schdulineguid = UUID.randomUUID().toString();
        Scheduledetail loc = new Scheduledetail();
        loc.locationadress = adress;
        loc.mobilenumber = Contactpersonnumbervalue;
        loc.tinnumber = tinnumber;
        loc.pan = pan;
        loc.landmark = nearestlandmarkvalue;
        loc.town = townclassvalue;
        loc.contactperson = contactpersonvalue;
        loc.othercompany = anyothercompanyvalue;
        loc.Closingday = closingday;
        loc.selectedproductcategories = selectedproductcategories;
        loc.alternatecustcode = "";
        loc.street = "";
        loc.area = "";
        loc.landLine = "";
        loc.shopname = "";
        loc.city = city;
        loc.pin = pin;
        loc.email = email;
        loc.Customercatogory = customercategory;
        loc.CustomercatogoryId = db.getcustomercategoriesId(customercategory);
        loc.locationname = custname;
        loc.marketname = marketname;
        loc.StartTime = startTime;
        loc.EndTime = endTime;
        loc.latitude = latitude;
        loc.longitude = longitude;
        loc.otpverified = otpverified;
        loc.needotp = "1";
        loc.otpsubtime=otpsubtime;
        db.editCustomer(scheduledetailid, loc, crtime);


        loc.Scheduledetailid = schdulineguid;
        if (selectedarraytext.length > 0) {
            db.deletesurrogates(scheduledetailid);
            for (int i = 0; i < selectedarraytext.length; i++) {
                String datas = selectedarraytext[i];
                boolean chk = selectedarray[i];
                if(chk == true && datas == null)
                {
                    db.insertproductinputs(scheduledetailid, categorycodearray[i],
                            "");
                }
                if (datas != null && datas != "") {
                    // Toast.makeText(AddLocationNewActivity.this, datas,
                    // Toast.LENGTH_LONG).show();
                    db.insertproductinputs(scheduledetailid, categorycodearray[i],
                            selectedarraytext[i]);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.outlinear);
        LayoutInflater layoutInflater = (LayoutInflater) SurrogatesEditActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popuplayout, null);
        TextView header = (TextView) customView.findViewById(R.id.header);
        header.setText(R.string.savecustomer);
        TextView msg = (TextView) customView.findViewById(R.id.message);
        msg.setText(R.string.savecustomermsg);
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                //super.onBackPressed();
                Intent intent = new Intent(SurrogatesEditActivity.this, LandingActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    public void intialisedialoguedata() throws JSONException {
        JSONArray data = db.getchecklist();
        productcategories = new String[data.length()];
        categorycode = new String[data.length()];
        selectedarray = new boolean[data.length()];
        typearray = new String[data.length()];
        selectedarraytext = new String[data.length()];

        for (int i = 0; i < data.length(); i++) {
            JSONObject dataobj = data.getJSONObject(i);
            productcategories[i] = dataobj.getString("catname");
            categorycode[i] = dataobj.getString("catid");
            selectedarray[i] = false;
            typearray[i] = dataobj.getString("type");
            // selectedarraytext[i]="";
        }
    }
}
