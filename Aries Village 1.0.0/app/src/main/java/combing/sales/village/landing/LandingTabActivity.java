package combing.sales.village.landing;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import combing.sales.village.ariesc.R;

public class LandingTabActivity extends AppCompatActivity {
    SharedPreferences pref;
    private static final int CAMERA_REQUEST = 1888;
    private static Uri mImageUri;
    private static String type = "";
    private Bitmap customerphoto;
    public static Activity mAct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {



      /*  super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_tab);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_tab_main);
        mAct = LandingTabActivity.this;
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        //=======================Table Layout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#f54525"));
        tabLayout.setTabTextColors(Color.parseColor("#000000"),Color.parseColor("#f54525"));
        tabLayout.addTab(tabLayout.newTab().setText("SUB-D"));
        tabLayout.addTab(tabLayout.newTab().setText("VILLAGE"));
        tabLayout.addTab(tabLayout.newTab().setText("CUSTOMER"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        final PagerAdapter adapter = new TabAdapterLanding
                (getApplicationContext(),getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        pref = getSharedPreferences("Config", MODE_PRIVATE);
        //String TabState=pref.getString("TabState", "SUB-D");
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                int pos= tab.getPosition();
                if (pos==2){

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }



        });

   /*     if(TabState.equals("Inward") )
        {
            viewPager.setCurrentItem(0);
        }
        else
        {
            viewPager.setCurrentItem(1);
        }
*/





    }
    public void onBackPressed() {
        pref = getSharedPreferences("Config", MODE_PRIVATE);
        Intent intent=new Intent(LandingTabActivity.this,LandingActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }
/*
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap mImageBitmap = (Bitmap) extras.get("data");
                ImageView proof;
                ImageView inside;
                if (type == "photo") {
                    customerphoto = getResizedBitmap(mImageBitmap, 500, 500);
                     proof = (ImageView) findViewById(R.id.addcust);
                     proof.setImageBitmap(customerphoto);
                }
            }
        } catch (Exception e) {
            // Toast.makeText(this, R.string.turn_off_high_quality_img, Toast.LENGTH_SHORT).show();
        }
    }*/
    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    public void setCurrentItem(int i, boolean b) {
    }
}
