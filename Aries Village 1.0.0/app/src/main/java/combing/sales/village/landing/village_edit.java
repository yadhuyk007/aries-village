package combing.sales.village.landing;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import combing.sales.village.ariesc.R;
import combing.sales.village.database.DatabaseHandler;
import combing.sales.village.gps.GPSAccuracy;
import combing.sales.village.objects.Font;
import combing.sales.village.objects.Global;

public class village_edit extends AppCompatActivity {

    private DatabaseHandler db;
    private static final int REQUEST_CODE_GPS_ACCURACY = 1 ;
    private static final int CAMERA_REQUEST = 1888;
    List<String> subdName = new ArrayList<String>();
    String name;
    private Activity activity;
    private Uri mImageUri;
    private String type;
    private Bitmap customerphoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_village_edit);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        Bundle bundle = getIntent().getExtras();
        name = bundle.getString("name","name");
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        try{
            intialisubds();
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            setcurrentdata();;
        }catch (Exception e){
            e.printStackTrace();
        }
        Button savesubd = (Button) findViewById(R.id.savenewvillage);

        ImageButton photo_camera = (ImageButton) findViewById(R.id.photo_camera);
        photo_camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(village_edit.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) village_edit.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                } else {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photo;
                    try {
                        photo = createTemporaryFile("picture", ".jpg");
                        photo.delete();
                    } catch (Exception e) {
                        Toast.makeText(village_edit.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mImageUri = Uri.fromFile(photo);
                    type = "photo";
                    startActivityForResult(intent, CAMERA_REQUEST);
                }
            }

            private File createTemporaryFile(String part, String ext) throws Exception {
                File tempDir = Environment.getExternalStorageDirectory();
                tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
                if (!tempDir.exists()) {
                    tempDir.mkdirs();
                }
                return File.createTempFile(part, ext, tempDir);
            }
        });

        savesubd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText savesubd = (EditText)findViewById(R.id.villagename);

                Intent intent = new Intent(village_edit.this,
                        GPSAccuracy.class);
                intent.putExtra("operation", "addlocation");
                startActivityForResult(intent, REQUEST_CODE_GPS_ACCURACY);
                //this.getActivity().overridePendingTransition(0, 0);
                //db.loadsubdtable(subddata);
                //take photo
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap mImageBitmap = (Bitmap) extras.get("data");
                ImageView proof;
                ImageView inside;

                if (type == "photo") {
                    customerphoto = getResizedBitmap(mImageBitmap, 500, 500);
                    proof = (ImageView)findViewById(R.id.addvillages);
                    proof.setImageBitmap(customerphoto);
                }
            }

            if (requestCode == 1) {
                if (resultCode == Activity.RESULT_OK) {
                    Double lati = data.getDoubleExtra("latit", 0);
                    Double longi = data.getDoubleExtra("longit", 0);
                    Float acc = data.getFloatExtra("minacc", 0);
                    String provi = data.getStringExtra("prov");
                    //String operation = data.getStringExtra("operation");
                    //String fixtime = data.getStringExtra("fixtime");
                    String gpsTime = data.getStringExtra("gpsTime");
                    // Toast.makeText(getApplicationContext(),
                    // lati + ", " + longi + ", " + provi + ", " + acc,
                    // Toast.LENGTH_LONG).show();
                    // gowithcurrent(lati, longi, acc, provi);
                    if (lati > 0 && longi > 0) {
                        saveData(lati, longi, acc, provi, gpsTime);
                    } else
                        Global.Toast((Activity) village_edit.this, "Location not found. Please try again!", Toast.LENGTH_LONG, Font.Regular);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void saveData(double lati, double longi, float acc, String provi, String fixtime) {
        if (lati > 0 && longi > 0) {

            String vname = ((TextView) findViewById(R.id.villagename)).getText().toString();
            Calendar c = Calendar.getInstance();
            DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            String crtime = dateTimeFormat.format(c.getTime());
            String subdguid = UUID.randomUUID().toString();

            Spinner spinner = findViewById(R.id.spinnervillagesubd);
            String subd = spinner.getSelectedItem().toString();
            String subdid = db.getsubdid(subd);

            db.updatevillage( vname,subdguid, lati + "", longi + "", crtime, subdid,name);
            if (customerphoto != null) {
                String filename = subdguid + ".png";
                createDirectoryAndsavetempfile(customerphoto, "village_" + filename);
            }
        }
        finishActivityFromHere();
    }
    private void finishActivityFromHere() {
        Intent intent = new Intent(village_edit.this, LandingTabActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }
    private void setcurrentdata() {
        try {
            JSONObject villdat = db.getvillage(name);
            String org = villdat.optString("subd","");
            String subdname = db.getsubdname(org);
            String vname = villdat.optString("villagename","");
            ((EditText)findViewById(R.id.villagename)).setText(vname);
            Spinner subdspin = (Spinner)findViewById(R.id.spinnervillagesubd);
            ArrayAdapter<String> subdadapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item,subdName){
                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    TextView tv = (TextView) super.getView(position, convertView, parent);
                    try {
                        tv.setTextColor(Color.parseColor("#000000"));
                        tv.setTextSize(12);

                        String fontPath = "fonts/segoeui.ttf";
                        Typeface m_typeFace = Typeface.createFromAsset(activity.getAssets(), fontPath);
                        tv.setTypeface(m_typeFace);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return tv;
                }
            };
            subdadapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
            subdspin.setAdapter(subdadapter);
            subdspin.setSelection(getsubdpos(subdname));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void intialisubds() throws JSONException {
        db = new DatabaseHandler(this);
        JSONArray subdarr = db.getSudDetails();
        subdName.add("SELECT");
        for (int i = 0; i < subdarr.length(); i++) {
            JSONObject obj = subdarr.optJSONObject(i);
            subdName.add(obj.optString("subdname"));
        }
    }
    private int getsubdpos(String subd) {

        int pos=0;
        int subdval=0;

        for(String s :subdName)
        {
            if(s.equals(subd))
            {
                subdval=pos;
                break;
            }
            pos++;
        }
        return subdval;
    }
    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }
}
