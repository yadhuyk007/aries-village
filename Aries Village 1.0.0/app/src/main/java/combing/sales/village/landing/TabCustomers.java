package combing.sales.village.landing;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import combing.sales.village.ariesc.R;
import combing.sales.village.customer.NewCustomerActivity;
import combing.sales.village.customerlist.CustListRecyclerViewAdapter;
import combing.sales.village.customerlist.CustomerList;
import combing.sales.village.customerlist.CustomerListActivity;
import combing.sales.village.customerlist.ExpandableRecyclerAdapter;
import combing.sales.village.customerlist.ScheduleAdapter;
import combing.sales.village.customerlist.ScheduledRouteObject;
import combing.sales.village.database.DatabaseHandler;
import combing.sales.village.gps.CopyOfVehicletracker;
import combing.sales.village.gps.PlayTracker;
import combing.sales.village.objects.Global;
import combing.sales.village.objects.Schedule;
import combing.sales.village.objects.Stacklogger;

import static android.content.Context.MODE_PRIVATE;

//public class TabCustomers extends AppCompatActivity {



@SuppressLint("ValidFragment")
public class TabCustomers extends Fragment implements SUBDAdapter.ItemClickListener {

    private Button custlist;
    private Menu menu;
    private static String salespersonid = "";
    private static String salespersonname = "";
    private static String Scheduledate = "0";
    CustListRecyclerViewAdapter adapter;
    FloatingActionButton fab1;
    LinearLayout fabLayoutNewCust;
    LinearLayout fabLayoutCustSrch;
    private boolean isFABOpen = false;
    private ScheduleAdapter mAdapter;
    private RecyclerView recyclerView;
    private ScheduledRouteObject[] elemts;
    private ScheduledRouteObject[] scheduledata;
    private DatabaseHandler db;
    private TextView searchText;
    private ImageView srchbtn;
    private SharedPreferences pref;


    //public class TabCustomers extends Activity  {
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view =  inflater.inflate(R.layout.schedule_customerlist_main, container, false);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(getActivity()));

        db = new DatabaseHandler(getContext());
        pref = getContext()
                .getSharedPreferences("Config",
                        MODE_PRIVATE);
        String salespersonsync = db.getrecentsynctime();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date temp = dateTimeFormat.parse(salespersonsync);
            dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            salespersonsync = dateTimeFormat.format(temp);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CAMERA
        };
        if(!hasPermissions(getActivity(), PERMISSIONS)){
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
        }
        startTimerService();
        String scheduleid = db.getactualscheduleid();
        String beginTime = db.getscheduleheaderbegintime(scheduleid);
        final FloatingActionButton add = (FloatingActionButton)view.findViewById(R.id.addnew);

        RecyclerView rv = (RecyclerView)view.findViewById(R.id.recyclerview);
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy<0 && !add.isShown())
                    add.show();
                else if(dy>0 && add.isShown())
                    add.hide();
            }
        });
        add.bringToFront();
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(getActivity(), NewCustomerActivity.class);
                startActivity(intent);
            }
        });
        try {
            String pos = String.valueOf(Global.spinnerpos);
            elemts = getScheduledRoutes("",pos);
            // mExpListAdapter = new ExpListAdapter(getApplicationContext(),
            // elemts, mArrChildelements, this);
            loadCustList(elemts,view);

            //check gps on
            LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            boolean statusOfGPS = manager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!statusOfGPS) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        getActivity());
                builder.setTitle(R.string.switch_on_gps);
                builder.setMessage(R.string.gps_off_alert_message);
                builder.setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                startActivityForResult(
                                        new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                        0);
                            }
                        });
                builder.show();
            }

            if (beginTime == null) {
                String schDate = db.GetCurrentScheduledate();
                Calendar c = Calendar.getInstance();
                dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd",
                        Locale.US);
                String currenttime = dateTimeFormat.format(c.getTime());
            }
        } catch (Exception e2) {
            // TODO Auto-generated catch block

            e2.printStackTrace();
        }

        return view;
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void startTimerService() {
        // Toast.makeText(getApplicationContext(), "startTimerService on sync",
        // Toast.LENGTH_LONG).show();
        // startService(new Intent(this, CopyOfVehicletracker.class));
        // startService(new Intent(this, Vehicletracker.class));
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(getActivity(),
                CopyOfVehicletracker.class);
        PendingIntent pintent = PendingIntent.getService(
                getActivity(), 0, intent, 0);
        AlarmManager alarm = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                1000, pintent);

        Intent intentn = new Intent(getActivity(), PlayTracker.class);
        PendingIntent pintentn = PendingIntent.getService(
                getActivity(), 0, intentn, 0);
        AlarmManager alarmn = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        alarmn.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                1000, pintentn);
    }

    @Override
    public void onItemClick(View view, int position) {

    }
    public void loadCustList(ScheduledRouteObject[] elemts, View view)
            throws Exception {
        final List<Schedule> schedules = new ArrayList<>();
        for (int i = 0; i < elemts.length; i++) {
            List<CustomerList> custlist = new ArrayList<>();
            ;

            try {

                String RouteId = elemts[i].RouteId;
                String RouteName = elemts[i].RouteName;
                String RouteTime = elemts[i].RouteTime;
                //child array list
                JSONArray custtemparr = elemts[i].CustomerList;
                for (int j = 0; j < custtemparr.length(); j++) {
                    JSONObject customerObject = new JSONObject();
                    customerObject = elemts[i].CustomerList
                            .getJSONObject(j);

                    String name = customerObject.getString("CustName");
                    String customercode = customerObject.getString("customercode");
                    String category = customerObject.getString("Customercategory");
                    String adr = customerObject.getString("CustAddress");
                    String invoice = customerObject.optString("otpverified","");
//                    String invoice = customerObject.getString("Invoice");
                    String scheduledetailid = customerObject.getString("scheduledetailid");
                    String latitude = customerObject.getString("latitude");
                    String longitude = customerObject.getString("longitude");
                    String approve = customerObject.getString("Approve");
                    String approveorder = customerObject.getString("ApproveOrder");
                    String status = customerObject.getString("Status");
                    String locationlock = customerObject.getString("locationlock");
                    String otpverify = customerObject.getString("otpverify");
                    CustomerList custitem = new CustomerList(name, customercode, category, adr, invoice, scheduledetailid, latitude, longitude,approve,approveorder,status,locationlock,otpverify);
                    custlist.add(custitem);

                }
                Schedule schObject = new Schedule(RouteName, custlist, RouteId, RouteTime);
                schedules.add(schObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // set up the RecyclerView
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        mAdapter = new ScheduleAdapter(getActivity(), schedules);

        mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @Override
            public void onListItemExpanded(int position) {
                Schedule expandedCustList = schedules.get(position);

                //// String toastMsg = getResources().getString(R.string.expanded, expandedMovieCategory.getName());
                //Toast.makeText(MainActivity.this,
                //    toastMsg,
                //   Toast.LENGTH_SHORT)
                //  .show();
            }

            @Override
            public void onListItemCollapsed(int position) {
                Schedule collapsedCustList = schedules.get(position);

                // String toastMsg = getResources().getString(R.string.collapsed, collapsedMovieCategory.getName());
                // Toast.makeText(MainActivity.this,
                //         toastMsg,
                //        Toast.LENGTH_SHORT)
                //        .show();
            }
        });
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        if (elemts.length == 1) {
            mAdapter.expandParent(0);

        }
    }
    public ScheduledRouteObject[] getScheduledRoutes(String text, String headid)
            throws Exception {

        DatabaseHandler db = new DatabaseHandler(getContext());

        JSONArray scheduleDetailDataArry = db.getscheduledata(text);
        SharedPreferences pref = getContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        salespersonid = pref.getString("personid", "");
        salespersonname = pref.getString("personname", "");


        if (scheduleDetailDataArry.length() > 0) {

            String Scheduledate_temp = scheduleDetailDataArry.getJSONObject(0)
                    .getString("scheduledate");

            DateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy");

            Date temp = new Date();
            try {
                temp = dateTimeFormat1.parse(Scheduledate_temp);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            DateFormat dateTimeFormat2 = new SimpleDateFormat("dd MMM  yyyy");

            salespersonid = pref.getString("personid", "");
            salespersonname = pref.getString("personname", "");
            Scheduledate = dateTimeFormat2.format(temp);

        }
        return getFormattedScheduleRouteData(scheduleDetailDataArry);
    }
    public ScheduledRouteObject[] getFormattedScheduleRouteData(
            JSONArray inputJsonArray) throws JSONException {
        JSONArray OutputJsonArr = new JSONArray();
        JSONObject inputJsonObject = null;
        JSONObject oJsonObject = null;
        ScheduledRouteObject[] ObjectItemData = new ScheduledRouteObject[inputJsonArray
                .length()];
        DatabaseHandler db = new DatabaseHandler(getContext());
        HashMap<String, String> invoicehashmap = db.getCustInvoice();

        for (int i = 0; i < inputJsonArray.length(); i++) {
            inputJsonObject = inputJsonArray.getJSONObject(i);

            JSONArray inCustomerListArray = inputJsonObject
                    .getJSONArray("coustemerlist");
            JSONArray outCustomerListArray = new JSONArray();
            for (int j = 0; j < inCustomerListArray.length(); j++) {

                JSONObject CustJSONObj = inCustomerListArray.getJSONObject(j);
                JSONObject CustomerObj = new JSONObject();
                CustomerObj.put("CustName",
                        CustJSONObj.getString("locationname"));
                CustomerObj.put("CustAddress",
                        CustJSONObj.getString("locationadress"));
                CustomerObj.put("Status", CustJSONObj.getString("status"));
                CustomerObj.put("Time", "");
                String custid = CustJSONObj.getString("locationid");
                CustomerObj.put("CustId", custid);
                CustomerObj.put("scheduledetailid",
                        CustJSONObj.getString("scheduledetailid"));
                CustomerObj.put("latitude", CustJSONObj.optString("latitude"));
                CustomerObj
                        .put("longitude", CustJSONObj.optString("longitude"));
                CustomerObj.put("Customercategory",
                        CustJSONObj.optString("Customercategory"));
                CustomerObj.put("customercode",
                        CustJSONObj.optString("customercode"));
                CustomerObj.put("locationlock",
                        CustJSONObj.optString("locationlock", "0"));
                CustomerObj.put("Approve",
                        CustJSONObj.optString("Approve", "0"));
                CustomerObj.put("ApproveOrder",
                        CustJSONObj.optString("ApproveOrder", "0"));
                if (invoicehashmap.containsKey(custid))
                    CustomerObj.put("Invoice", invoicehashmap.get(custid));
                else
                    CustomerObj.put("Invoice", "Invoice: NIL, Amount: 0");
                CustomerObj.put("otpverify",
                        CustJSONObj.optString("otpverified", "0"));
                outCustomerListArray.put(CustomerObj);
            }

            Calendar c = Calendar.getInstance();
            DateFormat dateTimeFormat = new SimpleDateFormat("hh:mm a");
            String formattedDate = dateTimeFormat.format(c.getTime());

            String temp = inputJsonObject.getString("headerstatus");
            ObjectItemData[i] = new ScheduledRouteObject(
                    inputJsonObject.getString("schedulepkid"),
                    inputJsonObject.getString("schedulename"), "",
                    outCustomerListArray,
                    inputJsonObject.getString("headerstatus"),
                    inputJsonObject.optString("schedulecopletionstatus",
                            "incomplete"));
        }
        scheduledata = ObjectItemData;

        return ObjectItemData;

    }

}

