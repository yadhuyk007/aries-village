package combing.sales.village.landing;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import combing.sales.village.ariesc.R;
import combing.sales.village.database.DatabaseHandler;
import combing.sales.village.gps.GPSAccuracy;
import combing.sales.village.objects.Font;
import combing.sales.village.objects.FourStrings;
import combing.sales.village.objects.Global;


public class TabVILLAGE extends Fragment implements VillageAdapter.ItemClickListener {
    VillageAdapter adapter;
    private HashMap<String, FourStrings> villagemap;
    private DatabaseHandler db;
    private List<String> subdList = new ArrayList<>();
    private static final int CAMERA_REQUEST = 1888;

    private static final int REQUEST_CODE_GPS_ACCURACY = 1;
    private static Uri mImageUri;
    private static String type = "";
    private Bitmap customerphoto;
    private View view;
    private Spinner spinner;
    List<String> subdName = new ArrayList<String>();
    private String subdval;
    private View mView;


    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.village, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.village_recycler_view);
        FloatingActionButton fabsubd = (FloatingActionButton) view.findViewById(R.id.fabvillage);
        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.VLinear2);
        linearLayout2.setVisibility(View.GONE);
        LinearLayout lLayout1 = (LinearLayout) view.findViewById(R.id.VLinear1);
        lLayout1.setVisibility(View.VISIBLE);

        try {
            intialisubds();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        fabsubd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.VLinear2);
                linearLayout2.setVisibility(View.VISIBLE);
                EditText text = (EditText)view.findViewById(R.id.villagename);
                text.setText("");
                LinearLayout linearLayout1 = (LinearLayout) view.findViewById(R.id.VLinear1);
                linearLayout1.setVisibility(View.GONE);
                final Button savesubd = (Button) view.findViewById(R.id.savenewvillage);

                //---------------------------------------

                Spinner spinner = (Spinner) view.findViewById(R.id.spinnervillagesubd);
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(TabVILLAGE.this.getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, subdName) {

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        TextView tv = (TextView) super.getView(position, convertView,
                                parent);
                        try {
                            tv.setTextColor(Color.parseColor("#000000"));
                            tv.setTextSize(9);

                            String fontPath = "fonts/segoeui.ttf";
                            Typeface m_typeFace = Typeface.createFromAsset(
                                    TabVILLAGE.this.getActivity().getApplicationContext().getAssets(), fontPath);
                            tv.setTypeface(m_typeFace);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return tv;
                    }

                };
                dataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
                spinner.setAdapter(dataAdapter);
                ImageButton photo_camera = (ImageButton) view.findViewById(R.id.photo_camera);
                photo_camera.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (ContextCompat.checkSelfPermission(TabVILLAGE.this.getActivity().getApplicationContext(),
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) TabVILLAGE.this.getActivity().getApplicationContext(),
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                        } else {
                            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                            File photo;
                            try {
                                photo = createTemporaryFile("picture", ".jpg");
                                photo.delete();
                            } catch (Exception e) {
                                Toast.makeText(TabVILLAGE.this.getActivity().getApplicationContext(), R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                                return;
                            }
                            mImageUri = Uri.fromFile(photo);
                            type = "photo";
                            startActivityForResult(intent, CAMERA_REQUEST);
                        }
                    }

                    private File createTemporaryFile(String part, String ext) throws Exception {
                        File tempDir = Environment.getExternalStorageDirectory();
                        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
                        if (!tempDir.exists()) {
                            tempDir.mkdirs();
                        }
                        return File.createTempFile(part, ext, tempDir);
                    }
                });

                savesubd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String villagename = ((TextView) view.findViewById(R.id.villagename)).getText().toString();
                        Spinner spin = view.findViewById(R.id.spinnervillagesubd);
                        String subd = spin.getSelectedItem().toString();
                        String valid = isvalid(villagename,subd,customerphoto);
                        if (!valid.contentEquals("TRUE")){
                            Toast.makeText(getContext(),valid,Toast.LENGTH_SHORT).show();
                        }else {
                            Intent intent = new Intent(TabVILLAGE.this.getActivity().getApplicationContext(),
                                    GPSAccuracy.class);
                            intent.putExtra("operation", "addlocation");
                            startActivityForResult(intent, REQUEST_CODE_GPS_ACCURACY);
                        }
                    }
                });
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity().getApplicationContext()));
        db = new DatabaseHandler(view.getContext());
        Object subd;
        try {
            villagemap = db.getVillagedData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        adapter = new VillageAdapter(TabVILLAGE.this.getActivity().getApplicationContext(), view, villagemap);

        recyclerView.setAdapter(adapter);

        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap mImageBitmap = (Bitmap) extras.get("data");
                ImageView proof;
                ImageView inside;

                if (type == "photo") {
                    customerphoto = getResizedBitmap(mImageBitmap, 500, 500);
                    proof = (ImageView) view.findViewById(R.id.addvillages);
                    proof.setImageBitmap(customerphoto);
                }
            }

            if (requestCode == 1) {
                if (resultCode == Activity.RESULT_OK) {
                    Double lati = data.getDoubleExtra("latit", 0);
                    Double longi = data.getDoubleExtra("longit", 0);
                    Float acc = data.getFloatExtra("minacc", 0);
                    String provi = data.getStringExtra("prov");
                    String gpsTime = data.getStringExtra("gpsTime");
                    if (lati > 0 && longi > 0) {
                        saveData(lati, longi, acc, provi, gpsTime);
                    } else
                        Global.Toast((Activity) TabVILLAGE.this.getActivity().getApplicationContext(),
                                "Location not found. Please try again!",
                                Toast.LENGTH_LONG, Font.Regular);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int locationaccuracy() {
        int locationMode = -1;
        try {
            locationMode = Settings.Secure.getInt(TabVILLAGE.this.getActivity().getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode;
    }

    public void saveData(double lati, double longi, float acc, String provi, String fixtime) {
        if (lati > 0 && longi > 0) {

            String villagename = ((TextView) view.findViewById(R.id.villagename)).getText().toString();
            Calendar c = Calendar.getInstance();
            DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            String crtime = dateTimeFormat.format(c.getTime());
            String subdguid = UUID.randomUUID().toString();

            spinner = view.findViewById(R.id.spinnervillagesubd);
            String subd = spinner.getSelectedItem().toString();

            String subdid = db.getsubdid(subd);
                db.insertvillagetable(subdguid, villagename, lati + "", longi + "", crtime, subdid);
                if (customerphoto != null) {
                    String filename = subdguid + ".png";
                    createDirectoryAndsavetempfile(customerphoto, "village_"
                            + filename);
                }
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.detach(TabVILLAGE.this).attach(TabVILLAGE.this).commit();
        }
    }
    private String isvalid(String villagename,String subd, Bitmap customerphoto) {
        String temp = "TRUE";
        DatabaseHandler db = new DatabaseHandler(getContext());


        if (villagename.contentEquals("")){
            return "Please Enter the Village Name";
        }
        boolean exist = db.villagesimilar(villagename);
        if (exist){
            return  "SubD with same name already exists,     Please try another Name";
        }
        if (subd.contentEquals("")||subd.contentEquals("SELECT")){
            return "Please Select a SubD";
        }
        if (customerphoto==null){
            return "Please Capture SubD Image";
        }

        return temp;
    }
    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");
        if (!direct.exists()) {
            direct.mkdirs();
        }
        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    private void intialisubds() throws JSONException {
        db = new DatabaseHandler(TabVILLAGE.this.getActivity().getApplicationContext());
        JSONArray subdarr = db.getSudDetails();
        subdName.add(getResources().getString(R.string.SELECT));
        for (int i = 0; i < subdarr.length(); i++) {
            JSONObject obj = subdarr.optJSONObject(i);
            subdName.add(obj.optString("subdname"));
        }
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}