package combing.sales.village.customer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

import combing.sales.village.ariesc.R;
import combing.sales.village.database.DatabaseHandler;
import combing.sales.village.gps.GPSAccuracy;
import combing.sales.village.gps.PlayTracker;
import combing.sales.village.landing.LandingActivity;
import combing.sales.village.objects.Font;
import combing.sales.village.objects.Global;
import combing.sales.village.objects.Locations;
import combing.sales.village.objects.Scheduledata;
import combing.sales.village.objects.Scheduledetail;
import combing.sales.village.objects.Stacklogger;

public class SurrogatesActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_GPS_ACCURACY = 1;
    public static String[] productcategories;
    public static boolean[] selectedarray;
    public static String[] categorycode;
    public static String[] typearray;
    public static String[] selectedarraytext;
    private String pin;
    private String phone;
    private String address;
    private String custcategory;
    private String custname;
    private DatabaseHandler db;
    private SharedPreferences pref;
    private String customerid = "";
    private String createtime = "";
    private Bitmap customerphoto;
    private Bitmap insidephoto;
    private boolean OTP_verificationstatus;
    private String OTP_submitTime;
    private String villageval;
    private String subdval;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surrogates);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));

        db = new DatabaseHandler(getApplicationContext());
        pref = getApplicationContext()
                .getSharedPreferences("Config",
                        MODE_PRIVATE);

        Bundle bundle = getIntent().getExtras();
        custname = bundle.getString("custname", "");
        address = bundle.getString("address", "");
        phone = bundle.getString("phone", "");
        villageval = bundle.getString("villageval","");
        pin = bundle.getString("pin", "");
        subdval = bundle.getString("subdval","");
        custcategory = bundle.getString("category","");
        OTP_verificationstatus = bundle.getBoolean("OTP_verificationstatus", false);
        OTP_submitTime = bundle.getString("OTP_submitTime", ""); 
        customerphoto = Global.sharedcustimg;
        insidephoto = Global.sharedinsideimg;

        ((Button) findViewById(R.id.next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });

        try {
            intialisedialoguedata();
        } catch (Exception e) {
            e.printStackTrace();
        }


        LinearLayout group = (LinearLayout)
                findViewById(R.id.outlinear);

        TextView customTitleView = new TextView(SurrogatesActivity.this);
        customTitleView.setText("SURROGATES");
        customTitleView.setTextSize(20);
        customTitleView.setPadding(20, 20, 20, 20);
        customTitleView.setBackgroundResource(R.color.gray2);
//        Font a = new Font(mAct, customTitleView, Font.Semibold);
//        builder.setCustomTitle(customTitleView);
        for (int ct = 0; ct < productcategories.length; ct++) {
            final int which = ct;
            if (typearray[ct] == null
                    || typearray[ct].contentEquals("")) {
                LayoutInflater inflater=getLayoutInflater();
                View child=inflater.inflate(R.layout.surrogate_checkbox,null);
                //final TextView first = new TextView(SurrogatesActivity.this);
                final TextView first=(TextView)child.findViewById(R.id.txt);
                first.setText(productcategories[ct]);
                //group.addView(first);
                //CheckBox firstCheck = new CheckBox(SurrogatesActivity.this);
                CheckBox firstCheck = (CheckBox)child.findViewById(R.id.chkbox);
                if (selectedarray[ct])
                    firstCheck.setChecked(true);

                // firstCheck.setGravity(Gravity.RIGHT);

                group.addView(child);
                // group.addView(firstCheck);
                firstCheck
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                CheckBox c = (CheckBox) v;
                                if (c.isChecked()) {
                                    // Toast.makeText(getActivity(),
                                    // "True", Toast.LENGTH_LONG)
                                    // .show();
                                    selectedarray[which] = true;
                                } else {
                                    selectedarray[which] = false;
                                }
                            }
                        });

            } else {
                LayoutInflater inflater=getLayoutInflater();
                View child=inflater.inflate(R.layout.surrogate_inputbox,null);
                //final TextView first = new TextView(SurrogatesActivity.this);
                final TextView first=(TextView)child.findViewById(R.id.txt);
                first.setText(productcategories[ct]);

                //group.addView(first);
                //final EditText firstEdit = new EditText(SurrogatesActivity.this);
                EditText firstEdit = (EditText)child.findViewById(R.id.chkbox);
                firstEdit.setText(selectedarraytext[ct]);
                if (typearray[ct].equalsIgnoreCase("Number"))
                    firstEdit.setInputType(InputType.TYPE_CLASS_PHONE);
                //group.addView(firstEdit);
                group.addView(child);
                firstEdit.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s,
                                              int start, int before, int count) {
                        // Toast.makeText(getActivity(), s + "",
                        // Toast.LENGTH_LONG).show();
                        selectedarray[which] = true;
                        selectedarraytext[which] = s + "";
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s,
                                                  int start, int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        // TODO Auto-generated method stub
                    }
                });
            }
        }
//        // *****************
//
//        builder.setPositiveButton("CLOSE",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog,
//                                        int which) {
//                        dialog.dismiss();
//                    }
//                });
//        final AlertDialog popupWindow = builder.create();
//        popupWindow.show();


    }

    public void next() {
        turnOnGPS();
        if (!PlayTracker.intime()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SurrogatesActivity.this);
            builder.setTitle(R.string.gps_unavailable);

            String lastset="";
            if(!Global.lastSetOn.contentEquals(""))
                lastset=Global.lastSetOn;

            builder.setMessage(getResources().getString(R.string.check_your_gps)+" \nGPS Fixed on: "+lastset);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                    }
             });

            AlertDialog dialog = builder.create();
            builder.show();

            return;
        }
        if (locationaccuracy() != 3) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SurrogatesActivity.this);
            builder.setTitle(R.string.enable_high_loc_accuracy);
            builder.setMessage(R.string.loc_acc_low);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    });

            builder.show();

            return;
        }

        Intent intent = new Intent(SurrogatesActivity.this,
                GPSAccuracy.class);
        intent.putExtra("operation", "addlocation");
        startActivityForResult(intent, REQUEST_CODE_GPS_ACCURACY);
        overridePendingTransition(0, 0);
    }

    private int locationaccuracy() {
        int locationMode = -1;
        try {
            locationMode = Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode;
    }

    private void turnOnGPS() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!statusOfGPS) {

            AlertDialog.Builder builder = new AlertDialog.Builder(SurrogatesActivity.this);
            builder.setTitle(R.string.switch_on_gps);
            builder.setMessage(R.string.gps_off_alert_message);


            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            startActivityForResult(
                                    new Intent(
                                            Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });


            builder.show();

            return;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // ****************GPS Changes**********************
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Double lati = data.getDoubleExtra("latit", 0);
                Double longi = data.getDoubleExtra("longit", 0);
                Float acc = data.getFloatExtra("minacc", 0);
                String provi = data.getStringExtra("prov");
                String operation = data.getStringExtra("operation");
                String fixtime = data.getStringExtra("fixtime");
                String gpsTime=data.getStringExtra("gpsTime");
                // Toast.makeText(getApplicationContext(),
                // lati + ", " + longi + ", " + provi + ", " + acc,
                // Toast.LENGTH_LONG).show();
                // gowithcurrent(lati, longi, acc, provi);
                if (lati > 0 && longi > 0)
                    saveData(lati, longi, acc, provi,gpsTime);
                else
                    Global.Toast(SurrogatesActivity.this,
                            "Location not found. Please try again!",
                            Toast.LENGTH_LONG, Font.Regular);
            }
            if (locationaccuracy() != 3) {
                AlertDialog.Builder builder = Global.Alert(SurrogatesActivity.this,
                        "Enable High Location Accuracy", "Currently location accuracy low.");
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                Global.setDialogFont(SurrogatesActivity.this, dialog);
                return;
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Global.Toast(SurrogatesActivity.this, "Cancelled!",
                        Toast.LENGTH_LONG, Font.Regular);
                // Write your code if there's no result
            }
        }

    }

    public void saveData(double lati, double longi, float acc, String provi,String fixtime) {
        if (lati > 0 && longi > 0) {
            String schbegintime = db.getschedulebegintime();
            if (schbegintime == null || schbegintime == "") {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.SECOND, (c.get(Calendar.SECOND) - 10));
                DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                String sync = dateTimeFormat.format(c.getTime());
                //db.intialisesynctable(sync);

                c = Calendar.getInstance();
                c.set(Calendar.SECOND, (c.get(Calendar.SECOND) - 5));
                schbegintime = dateTimeFormat.format(c.getTime());
                db.updatescheduleheaderbegintime(schbegintime,sync);

                SharedPreferences pref = getApplicationContext().getSharedPreferences(
                        "Config", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("Schbegintime", schbegintime);
                editor.commit();
                Log.e("Schedulebegins", schbegintime);
            }
            try {
                String uuid = UUID.randomUUID().toString();


                String name = custname;
                String latitude = lati + "";
                String longitude = longi + "";
                String custcode = "";
                String category = custcategory;
                String gstin = "";
                String landmark = "";
                String contactperson = "";
                String contactnumber = phone;
                String othercompanies = "";
                String town = "";
                String categoryids = "";
                if (!category.equals(""))
                    categoryids = db.getcustomercategoriesId(category);
                String pan = "";
                String email = subdval;
                boolean otpverified = OTP_verificationstatus;
                String locprovider = provi+"";
                String locaccuracy = acc+"";
                String directcov = "";
                String closingday = "";
                String storeclosed = "";
                boolean checkedpng = false;
                String branch = "";
                String drugLicienceNo = "";
                String locality = "";
                String city = villageval;
                String state = "";
                String coverageDay = "";
                String week1 = "";
                String week2 = "";
                String week3 = "";
                String week4 = "";
                String visitFrequency = "";
                String type = "";
                String wholeSale = "";
                String metro = "";
                String classification = "";
                String latitudePhoto = lati+"";
                String longitudePhoto = longi+"";
                String remarks = "";
                String marketname = "";
                String startTime = "";
                String endTime = "";
                String otpsubtime = OTP_submitTime;

                String productcats = "";
                String selectedproductcategories = "";
                for (int i = 0; i < selectedarray.length; i++) {
                    if (selectedarray[i]) {
                        selectedproductcategories += categorycode[i] + ",";
                    }
                }
                 if (!selectedproductcategories.equals(""))
                    productcats = selectedproductcategories;

                Addlocation(name, address, latitude, longitude, custcode, category,
                        gstin, landmark, contactperson, contactnumber,
                        othercompanies, town, productcats, categoryids, pan, email,
                        otpverified ? "1" : "0", locprovider, locaccuracy, uuid,
                        directcov, closingday, String.valueOf(storeclosed),
                        checkedpng ? "1" : "0", branch, drugLicienceNo, locality,
                        city, state, pin, coverageDay, week1, week2, week3, week4,
                        visitFrequency, type, wholeSale, metro, classification,
                        latitudePhoto, longitudePhoto, schbegintime, remarks,
                        marketname, selectedarraytext, categorycode,
                        startTime, endTime, otpsubtime,fixtime);
                if (customerphoto != null) {
                    String filename = uuid + ".png";
                    createDirectoryAndsavetempfile(customerphoto, "photo_"
                            + filename);
                }
                String uuid1 = UUID.randomUUID().toString();
                HashMap<String, Bitmap> photos = new HashMap<String, Bitmap>();
                photos.put(uuid1, insidephoto);
                if (photos != null && !customerid.equals("") && photos.size() > 0) {
                    db.insertDocumentPhotos(customerid, photos, createtime, "0");
                }
                if (photos != null && !customerid.equals("") && photos.size() > 0) {
                    for (String key : photos.keySet()) {
                        try {
                            Bitmap bitmap = photos.get(key);
                            String filename = key + ".png";
                            createDirectoryAndsavetempfile(bitmap, "document_"
                                    + filename);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(SurrogatesActivity.this, R.string.please_try_again, Toast.LENGTH_SHORT).show();

                try {
                    File tempDir = Environment.getExternalStorageDirectory();
                    tempDir = new File(tempDir.getAbsolutePath() + "/AriesErrors/");
                    if (!tempDir.exists()) {
                        tempDir.mkdirs();
                    }
                    Calendar c = Calendar.getInstance();
                    String myFormat = "dd-MM-yyyy HH:mm:ss";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    myFormat = "yyyyMMddHHmmss";
                    sdf = new SimpleDateFormat(myFormat, Locale.US);
                    String filedate = sdf.format(c.getTime());
                    File myExternalFile = new File(tempDir, "errorlog_" + filedate
                            + ".txt");
                    String error = filedate + "-----------";
                    error += e.toString();
                    FileOutputStream fos = new FileOutputStream(myExternalFile);
                    OutputStreamWriter osw = new OutputStreamWriter(fos);
                    osw.append(error);
                    osw.close();
                } catch (IOException ioe) {
                    // ...
                }
            }finally {
                finishActivityFromHere();
            }
        }
    }

    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void finishActivityFromHere() {
        Intent intent = new Intent(SurrogatesActivity.this, LandingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }


    public void Addlocation(String name, String adress, String latitude,
                            String longitude, String costomercode, String customercategory,
                            String tinnumber, String nearestlandmarkvalue,
                            String contactpersonvalue, String Contactpersonnumbervalue,
                            String anyothercompanyvalue, String townclassvalue,
                            String selectedproductcategories, String customercatogorycode,
                            String pan, String email, String otpverified, String locprovider,
                            String locaccuracy, String uuid, String directcov,
                            String closingday, String storeclosed, String pngcovered,
                            String branch, String drugLicienceNo, String locality, String city,
                            String state, String pin, String coverageDay, String week1,
                            String week2, String week3, String week4, String visitFrequency,
                            String type, String wholeSale, String metro, String classification,
                            String latitudePhoto, String longitudePhoto, String schbegintime,
                            String remarks, String marketname, String[] selectedarraytext,
                            String[] categorycodearray, String startTime, String endTime, String otpsubtime, String fixtime)
            throws Exception {
        Locations loctodb = new Locations();
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String crtime = dateTimeFormat.format(c.getTime());
        Log.e("CreatedCust", crtime);
        String schdulineguid = UUID.randomUUID().toString();

        loctodb.Locationname = name;
        loctodb.Locationadress = adress;
        loctodb.Latitude = latitude;
        loctodb.Longitude = longitude;
        loctodb.Locationid = UUID.randomUUID().toString();
        customerid = loctodb.Locationid;
        loctodb.createtime = crtime;
        createtime = crtime;
        loctodb.costomercode = costomercode;
        loctodb.customercategory = customercategory;
        loctodb.tinnumber = tinnumber;
        loctodb.nearestlandmarkvalue = nearestlandmarkvalue;
        loctodb.contactpersonvalue = contactpersonvalue;
        loctodb.Contactpersonnumbervalue = Contactpersonnumbervalue;
        loctodb.anyothercompanyvalue = anyothercompanyvalue;
        loctodb.townclassvalue = townclassvalue;
        loctodb.selectedproductcategories = selectedproductcategories;
        loctodb.customercatogorycode = customercatogorycode;
        loctodb.schdulineguid = schdulineguid;
        loctodb.pan = pan;
        loctodb.otpverified = otpverified;
        loctodb.locprovider = locprovider;
        loctodb.locaccuracy = locaccuracy;
        loctodb.email = email;
        loctodb.photouuid = uuid;
        loctodb.directcov = directcov;
        loctodb.closingday = closingday;
        loctodb.storeclosed = storeclosed;
        loctodb.pngcovered = pngcovered;
        loctodb.branch = branch;

        loctodb.drugLicienceNo = drugLicienceNo;
        loctodb.locality = locality;
        loctodb.city = city;
        loctodb.state = state;
        loctodb.pin = pin;
        loctodb.coverageDay = coverageDay;
        loctodb.week1 = week1;
        loctodb.week2 = week2;
        loctodb.week3 = week3;
        loctodb.week4 = week4;
        loctodb.visitFrequency = visitFrequency;
        loctodb.type = type;
        loctodb.wholeSale = wholeSale;
        loctodb.metro = metro;
        loctodb.classification = classification;
        loctodb.latitudePhoto = latitudePhoto;
        loctodb.longitudePhoto = longitudePhoto;
        loctodb.remarks = remarks;
        loctodb.marketname = marketname;
        loctodb.startTime = startTime;
        loctodb.endTime = endTime;
        loctodb.otpsubtime = otpsubtime;
        loctodb.fixtime=fixtime;

        db.addnewlocation(loctodb);

        String schedheadid = db.isnewlocationsexist();
        if (schedheadid.equals("0")) {
            Calendar c2 = Calendar.getInstance();
            DateFormat dateTimeFormat2 = new SimpleDateFormat("MM/dd/yyyy");
            String schdate = dateTimeFormat2.format(c2.getTime());
            SharedPreferences pref = getApplicationContext()
                    .getSharedPreferences("Config", MODE_PRIVATE);
            Scheduledata headerdatatodb = new Scheduledata();
            headerdatatodb.Scheduleid = UUID.randomUUID().toString();
            headerdatatodb.Schedulename = "New Locations";
            headerdatatodb.Salespersonid = pref.getString("personid", "0");
            headerdatatodb.salespersonname = pref.getString("personname", "");
            headerdatatodb.Routenetworkid = "0";
            headerdatatodb.Headerstatus = "";
            headerdatatodb.Scheduledate = schdate;
            headerdatatodb.begintime = schbegintime;

            Scheduledetail detail = new Scheduledetail();

            detail.Scheduledetailid = schdulineguid;
            detail.locationid = loctodb.Locationid;
            detail.locationname = loctodb.Locationname;
            detail.locationadress = loctodb.Locationadress;
            detail.sequencenumber = "1";
            detail.status = "pending";
            detail.latitude = loctodb.Latitude;
            detail.longitude = loctodb.Longitude;
            detail.Customercatogory = loctodb.customercategory;
            detail.CustomercatogoryId = db
                    .getcustomercategoriesId(loctodb.customercategory);
            detail.invoicenumber = "";
            detail.picklistnumber = "";
            detail.customercode = loctodb.costomercode;
            detail.locationlock = "0";
            detail.Closingday = "";
            detail.mobilenumber = loctodb.Contactpersonnumbervalue;
            detail.tinnumber = loctodb.tinnumber;
            detail.pan = loctodb.pan;
            detail.landmark = loctodb.nearestlandmarkvalue;
            detail.town = townclassvalue;
            detail.contactperson = loctodb.contactpersonvalue;
            detail.othercompany = loctodb.anyothercompanyvalue;
            detail.otpverified = otpverified;
            detail.locprovider = locprovider;
            detail.locaccuracy = locaccuracy;
            detail.photoUUID = uuid;
            detail.email = email;
            detail.directcov = directcov;
            detail.selectedproductcategories = selectedproductcategories;
            detail.Closingday = closingday;
            detail.storeclosed = storeclosed;
            detail.pngcovered = pngcovered;
            detail.branch = branch;

            detail.drugLicienceNo = drugLicienceNo;
            detail.locality = locality;
            detail.city = city;
            detail.state = state;
            detail.pin = pin;
            detail.coverageDay = coverageDay;
            detail.week1 = week1;
            detail.week2 = week2;
            detail.week3 = week3;
            detail.week4 = week4;
            detail.visitFrequency = visitFrequency;
            detail.type = type;
            detail.wholeSale = wholeSale;
            detail.metro = metro;
            detail.classification = classification;
            loctodb.marketname = marketname;
            detail.otpsubtime = otpsubtime;

            db.firstlocationadd(headerdatatodb, detail,crtime);
        } else {
            Scheduledetail detail = new Scheduledetail();
            detail.Scheduledetailid = schdulineguid;
            detail.locationid = loctodb.Locationid;
            detail.locationname = loctodb.Locationname;
            detail.locationadress = loctodb.Locationadress;
            detail.sequencenumber = "1";
            detail.status = "pending";
            detail.latitude = loctodb.Latitude;
            detail.longitude = loctodb.Longitude;
            detail.Customercatogory = loctodb.customercategory;
            detail.CustomercatogoryId = db
                    .getcustomercategoriesId(loctodb.customercategory);
            detail.invoicenumber = "";
            detail.picklistnumber = "";
            detail.customercode = loctodb.costomercode;
            detail.locationlock = "0";
            detail.Closingday = "";
            detail.mobilenumber = loctodb.Contactpersonnumbervalue;
            detail.tinnumber = loctodb.tinnumber;
            detail.pan = loctodb.pan;
            detail.landmark = loctodb.nearestlandmarkvalue;
            detail.town = townclassvalue;
            detail.contactperson = loctodb.contactpersonvalue;
            detail.othercompany = loctodb.anyothercompanyvalue;
            detail.otpverified = otpverified;
            detail.locprovider = locprovider;
            detail.locaccuracy = locaccuracy;
            detail.photoUUID = uuid;
            detail.email = email;
            detail.directcov = directcov;
            detail.selectedproductcategories = selectedproductcategories;
            detail.Closingday = closingday;
            detail.storeclosed = storeclosed;
            detail.pngcovered = pngcovered;
            detail.branch = branch;

            detail.drugLicienceNo = drugLicienceNo;
            detail.locality = locality;
            detail.city = city;
            detail.state = state;
            detail.pin = pin;
            detail.coverageDay = coverageDay;
            detail.week1 = week1;
            detail.week2 = week2;
            detail.week3 = week3;
            detail.week4 = week4;
            detail.visitFrequency = visitFrequency;
            detail.type = type;
            detail.wholeSale = wholeSale;
            detail.metro = metro;
            detail.classification = classification;
            loctodb.marketname = marketname;
            detail.otpsubtime = otpsubtime;

            db.addtoexistingnewlocations(schedheadid, detail, crtime);
        }
        if (selectedarraytext.length > 0) {
            for (int i = 0; i < selectedarraytext.length; i++) {
                String datas = selectedarraytext[i];
                boolean chk = selectedarray[i];
                if(chk == true && datas == null)
                {
                    db.insertproductinputs(schdulineguid, categorycodearray[i],
                            "");
                }
                if (datas != null) {
                    // Toast.makeText(AddLocationNewActivity.this, datas,
                    // Toast.LENGTH_LONG).show();
                    db.insertproductinputs(schdulineguid, categorycodearray[i],
                            selectedarraytext[i]);
                }
            }
        }
    }



    @Override
    public void onBackPressed() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.outlinear);
        LayoutInflater layoutInflater = (LayoutInflater) SurrogatesActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popuplayout, null);
        TextView header = (TextView) customView.findViewById(R.id.header);
        header.setText(R.string.savecustomer);
        TextView msg = (TextView) customView.findViewById(R.id.message);
        msg.setText(R.string.savecustomermsg);
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                //super.onBackPressed();
        Intent intent = new Intent(SurrogatesActivity.this, LandingActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    public void intialisedialoguedata() throws JSONException {
        JSONArray data = db.getchecklist();
        productcategories = new String[data.length()];
        categorycode = new String[data.length()];
        selectedarray = new boolean[data.length()];
        typearray = new String[data.length()];
        selectedarraytext = new String[data.length()];

        for (int i = 0; i < data.length(); i++) {
            JSONObject dataobj = data.getJSONObject(i);
            productcategories[i] = dataobj.getString("catname");
            categorycode[i] = dataobj.getString("catid");
            selectedarray[i] = false;
            typearray[i] = dataobj.getString("type");
            // selectedarraytext[i]="";
        }
    }
}
