package combing.sales.village.landing;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.bugsnag.android.Bugsnag;

import combing.sales.village.ariesc.R;
import combing.sales.village.objects.webconfigurration;

public class LandingTabActivity extends AppCompatActivity {
    SharedPreferences pref;
    private static final int CAMERA_REQUEST = 1888;
    private static Uri mImageUri;
    private static String type = "";
    private Bitmap customerphoto;
    public static Activity mAct;
    private boolean gps_enabled;
    private boolean network_enabled;
    private LocationManager lm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_tab_main);
        mAct = LandingTabActivity.this;
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        pref = getSharedPreferences("Config", MODE_PRIVATE);
        String perid = pref.getString("personid", "");
        String porgid = pref.getString("tmsOrgId", "");
        webconfigurration C = new webconfigurration(getApplicationContext());
        Bugsnag.init(getApplicationContext(),"d12422957abb0ffffd9f22a3fc46154c");
        Bugsnag.setUser(perid,porgid, C.user);
        try{
            TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
            tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#f54525"));
            tabLayout.setTabTextColors(Color.parseColor("#000000"),Color.parseColor("#f54525"));
            tabLayout.addTab(tabLayout.newTab().setText("SUB-D"));
            tabLayout.addTab(tabLayout.newTab().setText("VILLAGE"));
            tabLayout.addTab(tabLayout.newTab().setText("CUSTOMER"));
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
            final PagerAdapter adapter = new TabAdapterLanding
                    (getApplicationContext(),getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapter);

            //String TabState=pref.getString("TabState", "SUB-D");
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                    int pos= tab.getPosition();
                    if (pos==2){

                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }



            });
        }catch (Exception e){
            e.printStackTrace();
            Bugsnag.notify(e);
        }
    }
    public void onBackPressed() {
        try{
            pref = getSharedPreferences("Config", MODE_PRIVATE);
            Intent intent=new Intent(LandingTabActivity.this,LandingActivity.class);
            startActivity(intent);
            finish();
        }catch (Exception e){
            e.printStackTrace();
            Bugsnag.notify(e);
        }

    }
    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    public void setCurrentItem(int i, boolean b) {
    }
}
