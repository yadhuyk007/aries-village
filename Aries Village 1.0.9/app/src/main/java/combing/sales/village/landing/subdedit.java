package combing.sales.village.landing;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bugsnag.android.Bugsnag;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import combing.sales.village.ariesc.R;
import combing.sales.village.customer.SurrogatesEditActivity;
import combing.sales.village.database.DatabaseHandler;
import combing.sales.village.gps.GPSAccuracy;
import combing.sales.village.objects.Font;
import combing.sales.village.objects.Global;

public class subdedit extends AppCompatActivity {

    private static final int REQUEST_CODE_GPS_ACCURACY = 1 ;
    private static final int CAMERA_REQUEST = 1888;
    private static String type = "";
    private String subdname;
    private Uri mImageUri;
    private Bitmap customerphoto;
    List<String> subdtypes = new ArrayList<String>();
    DatabaseHandler db;
    String name;
    String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subdedit);
        db = new DatabaseHandler(getApplicationContext());
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        Bundle bundle = getIntent().getExtras();
        subdname = bundle.getString("name","");
        subdtypes.add("SELECT");
        subdtypes.add("Hygiene");
        subdtypes.add("Health");
        try{
            String string = subdname;
            String[] parts = string.split("-");
            name = parts[0];
            code = parts[1];
            try {
                setcurrentdata();
            }catch (Exception e){
                e.printStackTrace();
            }
            String code = parts[1]; // 034556

            EditText txt = (EditText)findViewById(R.id.subdname);
            txt.setText(name);
            EditText codetxt = (EditText)findViewById(R.id.subdcode);
            codetxt.setText(code);
            final Button savesubd = (Button)findViewById(R.id.savenewsubd);

            ImageButton photo_camera = (ImageButton) findViewById(R.id.photo_camera);
            photo_camera.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (ContextCompat.checkSelfPermission(subdedit.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) subdedit.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                    } else {
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        File photo;
                        try {
                            photo = createTemporaryFile("picture", ".jpg");
                            photo.delete();
                        } catch (Exception e) {
                            Toast.makeText(subdedit.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        mImageUri = Uri.fromFile(photo);
                        type = "photo";
                        startActivityForResult(intent, CAMERA_REQUEST);
                    }
                }

                private File createTemporaryFile(String part, String ext) throws Exception {
                    File tempDir = Environment.getExternalStorageDirectory();
                    tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
                    if (!tempDir.exists()) {
                        tempDir.mkdirs();
                    }
                    return File.createTempFile(part, ext, tempDir);
                }
            });

            savesubd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String subdname = ((TextView) findViewById(R.id.subdname)).getText().toString();
                    String subdcode = ((TextView) findViewById(R.id.subdcode)).getText().toString();
                    Spinner type = findViewById(R.id.subdtype);
                    String subdtype = type.getSelectedItem().toString();
                    String valid = isvalid(subdname,customerphoto,subdtype,subdcode);
                    if (!valid.contentEquals("TRUE")){
                        Toast.makeText(getApplicationContext(),valid,Toast.LENGTH_SHORT).show();
                    }else{
                        Intent intent = new Intent(subdedit.this, GPSAccuracy.class);
                        intent.putExtra("operation", "addlocation");
                        startActivityForResult(intent, REQUEST_CODE_GPS_ACCURACY);
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            Bugsnag.notify(e);
        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap mImageBitmap = (Bitmap) extras.get("data");
                ImageView proof;
                ImageView inside;

                if (type == "photo") {
                    customerphoto = getResizedBitmap(mImageBitmap, 500, 500);
                    proof = (ImageView)findViewById(R.id.addcust);
                    proof.setImageBitmap(customerphoto);
                }/* else if (type == "inside") {
                    insidephoto = getResizedBitmap(mImageBitmap, 500, 500);
                    inside = (ImageView) findViewById(R.id.inside);
                    inside.setImageBitmap(insidephoto);
                }*/
            }

            if (requestCode == 1) {
                if (resultCode == Activity.RESULT_OK) {
                    Double lati = data.getDoubleExtra("latit", 0);
                    Double longi = data.getDoubleExtra("longit", 0);
                    Float acc = data.getFloatExtra("minacc", 0);
                    String provi = data.getStringExtra("prov");
                    String operation = data.getStringExtra("operation");
                    String fixtime = data.getStringExtra("fixtime");
                    String gpsTime=data.getStringExtra("gpsTime");
                    // Toast.makeText(getApplicationContext(),
                    // lati + ", " + longi + ", " + provi + ", " + acc,
                    // Toast.LENGTH_LONG).show();
                    // gowithcurrent(lati, longi, acc, provi);
                    if (lati > 0 && longi > 0) {
                        saveData(lati, longi, acc, provi, gpsTime);
                        //Intent intent=new Intent(mAct.getApplicationContext(),TabVILLAGE.class);
                        //startActivity(intent);
                    }
                    else
                        Global.Toast((Activity)subdedit.this,
                                "Location not found. Please try again!",
                                Toast.LENGTH_LONG, Font.Regular);
                }
                if (locationaccuracy() != 3) {
                    AlertDialog.Builder builder = Global.Alert((Activity) subdedit.this,
                            "Enable High Location Accuracy", "Currently location accuracy low.");
                    builder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                    Global.setDialogFont((Activity)subdedit.this, dialog);
                    return;
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    Global.Toast((Activity)subdedit.this, "Cancelled!",
                            Toast.LENGTH_LONG, Font.Regular);
                    // Write your code if there's no result
                }
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }
    private int locationaccuracy() {
        int locationMode = -1;
        try {
            locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode;
    }
    public void saveData(double lati, double longi, float acc, String provi,String fixtime) {

            if (lati > 0 && longi > 0) {
                try{
                //EditText savesubd = (EditText) view.findViewById(R.id.subdname);
                DatabaseHandler db= new DatabaseHandler(getApplicationContext());
                String newsubdname = ((TextView) findViewById(R.id.subdname)).getText().toString();
                String subdcode = ((TextView) findViewById(R.id.subdcode)).getText().toString();
                Calendar c = Calendar.getInstance();
                DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
                String crtime = dateTimeFormat.format(c.getTime());
                String subdguid = UUID.randomUUID().toString();
                String string = subdname;
                String[] parts = string.split("-");
                String name = parts[0];
                String code = parts[1];
                //name.replace(" ","");
                String subdid = db.getsubdid(name,code);
                Spinner type = findViewById(R.id.subdtype);
                String subdtype = type.getSelectedItem().toString();
                db.updatesubdtable(subdname,subdguid,newsubdname,lati+ "",longi+ "",crtime,subdid,subdcode,subdtype);

                if (customerphoto != null) {
                    String filename = subdguid + ".png";
                    createDirectoryAndsavetempfile(customerphoto, "subd_"
                            + filename);
                }
                }catch (Exception e){
                    e.printStackTrace();
                    Bugsnag.notify(e);
                }
            }

        finishActivityFromHere();
    }

    private void finishActivityFromHere() {
        Intent intent = new Intent(subdedit.this, LandingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }
    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void setcurrentdata() throws Exception {
        try {
            JSONObject subd = db.getsubDatawithName(name,code);
            String type = subd.optString("type","");
            TextView sdtype = findViewById(R.id.sdtype);
            sdtype.setText(type);
            int pos = gettypepos(type);
            Spinner Sdtype = (Spinner)findViewById(R.id.subdtype);
            ArrayAdapter<String> typeadapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item,subdtypes){
                @NonNull
                @Override
                public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    TextView tv = (TextView) super.getView(position, convertView, parent);
                    try {
                        tv.setTextColor(Color.parseColor("#000000"));
                        tv.setTextSize(12);

                        String fontPath = "fonts/segoeui.ttf";
                        Typeface m_typeFace = Typeface.createFromAsset(getAssets(), fontPath);
                        tv.setTypeface(m_typeFace);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return tv;
                }
            };
            typeadapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
            Sdtype.setAdapter(typeadapter);
            Sdtype.setSelection(pos);

        } catch (JSONException e) {
            e.printStackTrace();
            Bugsnag.notify(e);
        }
    }
    private String isvalid(String subdname, Bitmap customerphoto,String type,String code) {
        String temp = "TRUE";
        if (subdname.contentEquals("")){
            return "Please Enter the SubD Name";
        }
        if (type.contentEquals("SELECT")||type.contentEquals("")){
            return "Please Choose SubD Type";
        }
//        boolean exist = db.subdsimilar(subdname,type,code);
//        if (exist){
//            return  "SubD already exists.";
//        }
        if (customerphoto==null){
            return "Please Capture SubD Image";
        }

        return temp;
    }
    private int gettypepos(String type) {

        int pos=0;
        int subdval=0;

        for(String s :subdtypes)
        {
            if(s.equals(type))
            {
                subdval=pos;
                break;
            }
            pos++;
        }
        return subdval;
    }
}
