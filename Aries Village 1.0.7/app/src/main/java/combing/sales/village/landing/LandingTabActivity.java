package combing.sales.village.landing;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import combing.sales.village.ariesc.R;

public class LandingTabActivity extends AppCompatActivity {
    SharedPreferences pref;
    private static final int CAMERA_REQUEST = 1888;
    private static Uri mImageUri;
    private static String type = "";
    private Bitmap customerphoto;
    public static Activity mAct;
    private boolean gps_enabled;
    private boolean network_enabled;
    //private LandingActivity.LocationResult locationResult;
    private LocationManager lm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing_tab_main);
        mAct = LandingTabActivity.this;
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
//        lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
//        try {
//            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
//        } catch (Exception ex) {
//            Toast.makeText(getApplicationContext(), "GPS Provider not enabled", Toast.LENGTH_LONG).show();
//        }
//        try {
//            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//        } catch (Exception ex) {
//            Toast.makeText(getApplicationContext(), "Network Provider Not Enables", Toast.LENGTH_LONG).show();
//        }
//        if (gps_enabled)
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//                return;
//            }
//
//        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
//        if (network_enabled)
//            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
        //=======================Table Layout
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#f54525"));
        tabLayout.setTabTextColors(Color.parseColor("#000000"),Color.parseColor("#f54525"));
        tabLayout.addTab(tabLayout.newTab().setText("SUB-D"));
        tabLayout.addTab(tabLayout.newTab().setText("VILLAGE"));
        tabLayout.addTab(tabLayout.newTab().setText("CUSTOMER"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        final PagerAdapter adapter = new TabAdapterLanding
                (getApplicationContext(),getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        pref = getSharedPreferences("Config", MODE_PRIVATE);
        //String TabState=pref.getString("TabState", "SUB-D");
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                int pos= tab.getPosition();
                if (pos==2){

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }



        });

   /*     if(TabState.equals("Inward") )
        {
            viewPager.setCurrentItem(0);
        }
        else
        {
            viewPager.setCurrentItem(1);
        }
*/





    }
//    LocationListener locationListenerGps = new LocationListener() {
//        public void onLocationChanged(Location location) {
//            // locationResult.gotLocation(location);
//            lm.removeUpdates(this);
//            lm.removeUpdates(locationListenerNetwork);
//        }
//
//        public void onProviderDisabled(String provider) {
//        }
//
//        public void onProviderEnabled(String provider) {
//        }
//
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//        }
//    };

//    LocationListener locationListenerNetwork = new LocationListener() {
//        public void onLocationChanged(Location location) {
//            //  locationResult.gotLocation(location);
//            lm.removeUpdates(this);
//            lm.removeUpdates(locationListenerGps);
//        }
//
//        public void onProviderDisabled(String provider) {
//        }
//
//        public void onProviderEnabled(String provider) {
//        }
//
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//        }
//    };
    public void onBackPressed() {
        pref = getSharedPreferences("Config", MODE_PRIVATE);
        Intent intent=new Intent(LandingTabActivity.this,LandingActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }
/*
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap mImageBitmap = (Bitmap) extras.get("data");
                ImageView proof;
                ImageView inside;
                if (type == "photo") {
                    customerphoto = getResizedBitmap(mImageBitmap, 500, 500);
                     proof = (ImageView) findViewById(R.id.addcust);
                     proof.setImageBitmap(customerphoto);
                }
            }
        } catch (Exception e) {
            // Toast.makeText(this, R.string.turn_off_high_quality_img, Toast.LENGTH_SHORT).show();
        }
    }*/
    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    public void setCurrentItem(int i, boolean b) {
    }
}
