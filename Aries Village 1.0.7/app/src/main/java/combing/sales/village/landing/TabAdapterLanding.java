package combing.sales.village.landing;


import android.content.Context;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import combing.sales.village.customerlist.CustomerListActivity;

public class TabAdapterLanding extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    Context mcontext;

    public TabAdapterLanding(Context mcontext, FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.mcontext=mcontext;
    }

    public TabAdapterLanding(FragmentManager fm) {
        super(fm);
    }



    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabSUBD tab1 = new TabSUBD();
                return tab1;
            case 1:
                TabVILLAGE tab2 = new TabVILLAGE();
                return tab2;
            case 2:
                TabCustomers tab3 = new TabCustomers();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
