package combing.sales.village.landing;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import combing.sales.village.ariesc.R;
import combing.sales.village.database.DatabaseHandler;
import combing.sales.village.gps.GPSAccuracy;
import combing.sales.village.objects.Font;
import combing.sales.village.objects.FourStrings;
import combing.sales.village.objects.Global;

public class TabSUBD extends Fragment implements SUBDAdapter.ItemClickListener {
    SUBDAdapter adapter;
    private HashMap<String, FourStrings> subdmap;
    private DatabaseHandler db;
    private List<String> subdList = new ArrayList<>();
    private static final int CAMERA_REQUEST = 1888;

    private static final int REQUEST_CODE_GPS_ACCURACY = 1;
    private static Uri mImageUri;
    private static String type = "";
    private Bitmap customerphoto;
    private View view;
    private TextView searchText;
    List<String> subdtypes = new ArrayList<String>();
    SharedPreferences pref;
    private ImageView srchbtn;
    //private Activity mAct = this.getApplicationContext();


    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.subd_village, container, false);
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.subd_recycler_view);
        final FloatingActionButton fabsubd = (FloatingActionButton) view.findViewById(R.id.fabsubd);
        LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.Linear2);
        linearLayout2.setVisibility(View.GONE);
        LinearLayout lLayout1 = (LinearLayout) view.findViewById(R.id.Linear1);
        lLayout1.setVisibility(View.VISIBLE);
        pref = getContext().getSharedPreferences("Config",
                Context.MODE_PRIVATE);
        final FloatingActionButton add = (FloatingActionButton)view.findViewById(R.id.fabsubd);
        subdtypes.add("SELECT");
        subdtypes.add("Hygiene");
        subdtypes.add("Health");
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy<0 && !add.isShown())
                    add.show();
                else if(dy>0 && add.isShown())
                    add.hide();
            }
        });
        fabsubd.setOnClickListener(new View.OnClickListener() {
                                        @SuppressLint("RestrictedApi")
                                        @Override
                                        public void onClick(View v) {

                                           // final View customView = inflater.inflate(R.layout.add_subd, null);
                                            FloatingActionButton fb = view.findViewById(R.id.fabsubd);
                                            fb.setVisibility(View.GONE);
                                            LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.Linear2);
                                            linearLayout2.setVisibility(View.VISIBLE);
                                            EditText name = (EditText)view.findViewById(R.id.subdname);
                                            EditText code = (EditText)view.findViewById(R.id.subdcode);
                                            name.setText("");
                                            code.setText("");
                                            LinearLayout linearLayout1 = (LinearLayout) view.findViewById(R.id.Linear1);
                                            linearLayout1.setVisibility(View.GONE);
                                            //LinearLayout LinearLayoutView = (LinearLayout) customView.findViewById(R.id.linearLayouts1);

                                            final Button savesubd = (Button) view.findViewById(R.id.savenewsubd);

                                            ImageButton photo_camera = (ImageButton) view.findViewById(R.id.photo_camera);
                                            photo_camera.setOnClickListener(new View.OnClickListener() {

                                                @Override
                                                public void onClick(View v) {
                                                    if (ContextCompat.checkSelfPermission(TabSUBD.this.getActivity().getApplicationContext(),
                                                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                                            != PackageManager.PERMISSION_GRANTED) {
                                                        ActivityCompat.requestPermissions((Activity) TabSUBD.this.getActivity().getApplicationContext(),
                                                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                                                    } else {
                                                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                                                        File photo;
                                                        try {
                                                            photo = createTemporaryFile("picture", ".jpg");
                                                            photo.delete();
                                                        } catch (Exception e) {
                                                            Toast.makeText(TabSUBD.this.getActivity().getApplicationContext(), R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                                                            return;
                                                        }
                                                        mImageUri = Uri.fromFile(photo);
                                                        type = "photo";
                                                        startActivityForResult(intent, CAMERA_REQUEST);
                                                    }
                                                }

                                                private File createTemporaryFile(String part, String ext) throws Exception {
                                                    File tempDir = Environment.getExternalStorageDirectory();
                                                    tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
                                                    if (!tempDir.exists()) {
                                                        tempDir.mkdirs();
                                                    }
                                                    return File.createTempFile(part, ext, tempDir);
                                                }
                                            });

                                            savesubd.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    String subdname = ((TextView) view.findViewById(R.id.subdname)).getText().toString();
                                                    String subcode = ((TextView) view.findViewById(R.id.subdcode)).getText().toString();
                                                    Spinner type = view.findViewById(R.id.subdtype);
                                                    String subdtype = type.getSelectedItem().toString();
                                                    String valid = isvalid(subdname,customerphoto,subdtype,subcode);
                                                    if (!valid.contentEquals("TRUE")){
                                                        Toast.makeText(getContext(),valid,Toast.LENGTH_SHORT).show();
                                                    }else{
                                                        Intent intent = new Intent(TabSUBD.this.getActivity().getApplicationContext(),
                                                                GPSAccuracy.class);
                                                        intent.putExtra("operation", "addlocation");
                                                        startActivityForResult(intent, REQUEST_CODE_GPS_ACCURACY);
                                                    }


                                                    //this.getActivity().overridePendingTransition(0, 0);
                                                    //db.loadsubdtable(subddata);
                                                    //take photo
                                                }
                                            });
                                            //LinearLayoutView.setLayoutManager(new LinearLayoutManager(TabSUBD.this.getActivity().getApplicationContext()));
                                            //LinearLayoutView.setAdapter(adapter);*/
                                        }
                                    });

        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity().getApplicationContext()));
        db = new DatabaseHandler(view.getContext());
        Object subd;
        try {
            subdmap = db.getsubddata();
        } catch (JSONException e) {
            e.printStackTrace();
        }
       /* for (String key : subdmap.keySet()) {
            subdList.add(key);
        }*/
        adapter = new SUBDAdapter(TabSUBD.this.getActivity().getApplicationContext(),view,subdmap);
        recyclerView.setAdapter(adapter);

        searchText = (TextView) view.findViewById(R.id.searchText);
        srchbtn = (ImageView) view.findViewById(R.id.srchbtn);
        srchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchText.setText("");

            }
        });
        Spinner spinner = (Spinner) view.findViewById(R.id.subdtype);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(TabSUBD.this.getActivity().getApplicationContext(), android.R.layout.simple_spinner_item, subdtypes) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(12);

                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            TabSUBD.this.getActivity().getApplicationContext().getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }

        };
        dataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        spinner.setAdapter(dataAdapter);

        searchText.addTextChangedListener(new TextWatcher() {
        JSONArray data = new JSONArray();
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.length() > 0) {
                    srchbtn.setImageResource(R.drawable.ic_dhi_deletetextn);
                } else {
                    srchbtn.setImageResource(R.drawable.ic_dhi_new_search);
                }
                try {
                    subdmap = db.getSubdwithsearch(s.toString());
                    adapter = new SUBDAdapter(TabSUBD.this.getActivity().getApplicationContext(),view,subdmap);
                    recyclerView.setAdapter(adapter);
                  //loadsubd(data,view);
                } catch (Exception e2) {
                    e2.printStackTrace();

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        return view;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap mImageBitmap = (Bitmap) extras.get("data");
                ImageView proof;
                ImageView inside;

                if (type == "photo") {
                    customerphoto = getResizedBitmap(mImageBitmap, 500, 500);
                    proof = (ImageView) view.findViewById(R.id.addcust);
                    proof.setImageBitmap(customerphoto);
                }
            }

            if (requestCode == 1) {
                if (resultCode == Activity.RESULT_OK) {
                    Double lati = data.getDoubleExtra("latit", 0);
                    Double longi = data.getDoubleExtra("longit", 0);
                    Float acc = data.getFloatExtra("minacc", 0);
                    String provi = data.getStringExtra("prov");
                    String operation = data.getStringExtra("operation");
                    String fixtime = data.getStringExtra("fixtime");
                    String gpsTime=data.getStringExtra("gpsTime");
                    // Toast.makeText(getApplicationContext(),
                    // lati + ", " + longi + ", " + provi + ", " + acc,
                    // Toast.LENGTH_LONG).show();
                    // gowithcurrent(lati, longi, acc, provi);
                    if (lati > 0 && longi > 0) {
                        saveData(lati, longi, acc, provi, gpsTime);
                        //Intent intent=new Intent(mAct.getApplicationContext(),TabVILLAGE.class);
                        //startActivity(intent);
                    }
                    else
                        Global.Toast((Activity) TabSUBD.this.getActivity().getApplicationContext(),
                                "Location not found. Please try again!",
                                Toast.LENGTH_LONG, Font.Regular);
                }
                if (locationaccuracy() != 3) {
                    AlertDialog.Builder builder = Global.Alert((Activity) TabSUBD.this.getActivity().getApplicationContext(),
                            "Enable High Location Accuracy", "Currently location accuracy low.");
                    builder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                    Global.setDialogFont((Activity) TabSUBD.this.getActivity().getApplicationContext(), dialog);
                    return;
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    Global.Toast((Activity) TabSUBD.this.getActivity().getApplicationContext(), "Cancelled!",
                            Toast.LENGTH_LONG, Font.Regular);
                    // Write your code if there's no result
                }
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private int locationaccuracy() {
        int locationMode = -1;
        try {
            locationMode = Settings.Secure.getInt(TabSUBD.this.getActivity().getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode;
    }
    public void saveData(double lati, double longi, float acc, String provi,String fixtime) {
        if (lati > 0 && longi > 0) {
            String subdname = ((TextView) view.findViewById(R.id.subdname)).getText().toString();
            String subdcode = ((TextView) view.findViewById(R.id.subdcode)).getText().toString();
            Calendar c = Calendar.getInstance();
            DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            String crtime = dateTimeFormat.format(c.getTime());
            Spinner type = view.findViewById(R.id.subdtype);
            String subdtype = type.getSelectedItem().toString();
            String subdguid = UUID.randomUUID().toString();
            String person = pref.getString("personid","");
                db.insertsubdtable(subdguid,subdname,lati+ "",longi+ "",crtime,subdcode,subdtype,person);
                if (customerphoto != null) {
                    String filename = subdguid + ".png";
                    createDirectoryAndsavetempfile(customerphoto, "subd_"
                            + filename);
                }

//                FragmentTransaction ft = getFragmentManager().beginTransaction();
//                ft.detach(TabSUBD.this).attach(TabSUBD.this).commit();

            Intent intent=new Intent(LandingTabActivity.mAct,LandingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                    Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }
    }



    private String isvalid(String subdname, Bitmap customerphoto,String type,String code) {
        String temp = "TRUE";
        DatabaseHandler db = new DatabaseHandler(getContext());
        if (subdname.contentEquals("")){
            return "Please Enter the SubD Name";
        }
        if (code.contentEquals("")){
            return "Please Enter the SubD Code";
        }
        boolean exist = db.subdsimilar(subdname,type);
        if (exist){
            return  "SubD with same name already exists,    Please try another Name";
        }
        if (type.contentEquals("SELECT")||type.contentEquals("")){
            return "Please Choose SubD Type";
        }
        if (customerphoto==null){
            return "Please Capture SubD Image";
        }

        return temp;
    }

    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }
    @Override
    public void onItemClick(View view, int position) {

    }
}
