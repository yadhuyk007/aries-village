package combing.sales.village.landing;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import combing.sales.village.ariesc.R;
import combing.sales.village.database.DatabaseHandler;
import combing.sales.village.objects.FourStrings;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class VillageAdapter extends RecyclerView.Adapter<VillageAdapter.ViewHolder> {


    Context mcontext;
    View mview;
    private HashMap<String, FourStrings> villagemap;
    private LayoutInflater mInflater;
    private DatabaseHandler db;

    VillageAdapter(Context context, View v, HashMap<String, FourStrings> villagemap) {
        this.mInflater = LayoutInflater.from(context);
        Log log;
        this.mview = v;
        this.mcontext = context;
        this.villagemap = villagemap;
        db = new DatabaseHandler(context);

    }

    // inflates the row layout from xml when needed
    @Override
    public VillageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = mInflater.inflate(R.layout.village_row_mod, parent, false); // make_payment_row payment_approve_row
        return new VillageAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(VillageAdapter.ViewHolder holder, int position) {
        holder.myTextView.setText(getItemIds(position));
        String lat = villagemap.get(String.valueOf(position)).rate;
        if (lat.contentEquals("")||lat == null||lat.contentEquals("0.0")){
            holder.loc.setVisibility(View.GONE);
        }
        holder.setIsRecyclable(false);
    }

    private String getItemName(int position) {
        return villagemap.get(Integer.toString(position)).qty;

    }
    private String getItemIds(int position) {
        return villagemap.get(Integer.toString(position)).mrp;
    }

    // total number of rowsholder.

    @Override
    public int getItemCount() {
        return villagemap.size();
    }

    // convenience method for getting data at click position
    FourStrings getItem(int id) {
        return villagemap.get(id);
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        ImageView loc;
        String subdid;

        ViewHolder(final View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.itemname);

            myTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = getItemIds(getAdapterPosition());
                    Intent intent =  new Intent(mview.getContext(),village_edit.class);
                    intent.putExtra("name",name);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
            });

            loc = (ImageView) itemView.findViewById(R.id.location);

            loc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String Lat="";
                    String Longi = "";
                    JSONObject villagelatlog= new JSONObject();
                    String villageids = getItemName(getAdapterPosition());
                    try {
                        villagelatlog = db.getvillageLatLongs(villageids);
                        Lat = villagelatlog.getString("Lat");
                        Longi = villagelatlog.getString("Longs");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (!Lat.toString().equals("")
                            && !Lat.toString().equals("0")
                            && !Lat.toString().equals("0.0") && !Longi.toString().equals("")
                            && !Longi.toString().equals("0")
                            && !Longi.toString().equals("0.0")) {

                        Uri gmmIntentUri = Uri.parse("google.navigation:q="
                                + Lat + "," + Longi);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                                gmmIntentUri);
                        mapIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        mapIntent
                                .setPackage("com.google.android.apps.maps");
                        v.getContext().startActivity(mapIntent);
                    } else {
                        Toast.makeText(v.getContext(),
                                "No Location Data Found..!",
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
        }


        @Override
        public void onClick(View v) {

            // if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());

        }
    }



}
