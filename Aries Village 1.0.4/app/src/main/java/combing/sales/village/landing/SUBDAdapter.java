package combing.sales.village.landing;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import combing.sales.village.ariesc.R;
import combing.sales.village.database.DatabaseHandler;
import combing.sales.village.objects.FourStrings;


public class SUBDAdapter extends RecyclerView.Adapter<SUBDAdapter.ViewHolder> {

    Context mcontext;
    View mview;
    private HashMap<String, FourStrings> subdmap;
    private LayoutInflater mInflater;
    private DatabaseHandler db;


    // data is passed into the constructor
//    InwardOutwardAdapter(Context context, List<String> data, HashMap<String, Product> data1,HashMap<String, String> qtymap,View v ) {
    SUBDAdapter(Context context, View v, HashMap<String, FourStrings> subdmap) {
        this.mInflater = LayoutInflater.from(context);
        Log log;
        this.mcontext = context;
        this.mview = v;
        this.subdmap = subdmap;
        db = new DatabaseHandler(context);

    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = mInflater.inflate(R.layout.subd_row_mod, parent, false); // make_payment_row payment_approve_row
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.myTextView.setText(getItemName(position));
        String date = subdmap.get(String.valueOf(position)).rate;
        String lat = subdmap.get(String.valueOf(position)).packageType;
        if (lat.contentEquals("")||lat == null||lat.contentEquals("0.0")){
            holder.loc.setVisibility(View.GONE);
        }
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
        String currdate = dateTimeFormat.format(c.getTime());
        if (date!=null){
            if (date.contentEquals(currdate)){
                holder.visit.setVisibility(View.VISIBLE);
                holder.filler.setVisibility(View.GONE);
            }
        }
        holder.setIsRecyclable(false);

    }

    private String getItemName(int position) {
       // return "";
        return subdmap.get(Integer.toString(position)).mrp;
    }

    private String getItemIds(int position) {
        return subdmap.get(Integer.toString(position)).qty;
    }

    private String getItemDate(int position){
        return subdmap.get(Integer.toString(position)).rate;
    }

    // total number of rowsholder.
    @Override
    public int getItemCount() {
        return subdmap.size();
    }

    // convenience method for getting data at click position
    FourStrings getItem(int id) {
        return subdmap.get(id);
    }




    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        ImageView loc;
        ImageView filler;
        ImageView visit;
        String subdid;

        ViewHolder(final View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.itemname);

            myTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = getItemName(getAdapterPosition());
                    Intent intent =  new Intent(mview.getContext(),subdedit.class);
                    intent.putExtra("name",name);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mcontext.startActivity(intent);
                }
            });
            filler = itemView.findViewById(R.id.filler);
            visit = itemView.findViewById(R.id.visit);
            loc = (ImageView) itemView.findViewById(R.id.location);

            loc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String Lat="";
                    String Longi = "";
                    JSONObject subdlatlog= new JSONObject();
                    String subdids = getItemIds(getAdapterPosition());
                    try {
                        subdlatlog = db.getsubdLatLongs(subdids);
                        Lat = subdlatlog.getString("Lat");
                        Longi = subdlatlog.getString("Longs");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (!Lat.toString().equals("")
                            && !Lat.toString().equals("0")
                            && !Lat.toString().equals("0.0") && !Longi.toString().equals("")
                            && !Longi.toString().equals("0")
                            && !Longi.toString().equals("0.0")) {

                        Uri gmmIntentUri = Uri.parse("google.navigation:q="
                                + Lat + "," + Longi);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                                gmmIntentUri);
                        mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mapIntent
                                .setPackage("com.google.android.apps.maps");
                        v.getContext().startActivity(mapIntent);
                    } else {
                        Toast.makeText(v.getContext(),
                                "No Location Data Found..!",
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
        }


        @Override
        public void onClick(View v) {

        }
    }
}